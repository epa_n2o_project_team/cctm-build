#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Get information about bias/differences between my/generated and reference/tarballed output data.
### Currently drives {`ncdump -h`, bias_probe.ncl}, is driven by bias_probe_driver_driver.sh

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### Get these out of the way before we get to work:
THIS="$0"
# THIS_DIR="$(dirname ${THIS})"
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

## logging-related
DATETIME_FORMAT='%Y%m%d_%H%M'
DATETIME_STAMP="$(date +${DATETIME_FORMAT})"
LOG_FN="${THIS_FN}_${DATETIME_STAMP}.log"
# log is
# * local to this script, not build space
# * can be overridden by caller
if [[ -z "${LOG_FP}" ]] ; then
  export LOG_FP="${THIS_DIR}/${LOG_FN}"
fi

### Define sources of raw data and reference data, and sink for output/bias data.
### Can be overridden by caller.
CMAQ_ROOT='/project/inf35w/roche/CMAQ-5.0.1'
if [[ -z "${RAW_DATA_DIR}" ]] ; then
  RAW_DATA_DIR="${CMAQ_ROOT}/git/old/RUN_CCTM_CMAQv5_0_1.0"
fi
if [[ -z "${REF_DATA_DIR}" ]] ; then
  REF_DATA_DIR="${CMAQ_ROOT}/tarsplat/CMAQv5.0.1/data/ref/cctm"
fi
if [[ -z "${BIAS_DATA_DIR}" ]] ; then
  BIAS_DATA_DIR="${RAW_DATA_DIR}"
fi

# oh the aggravations of parsing CMAQ datafiles: more below
REF_DATA_FN_PRE_PREFIX='CCTM_V5g_par_Linux2_x86_64gfort'
REF_DATA_FN_PREFIX1="${REF_DATA_FN_PRE_PREFIX}."
REF_DATA_FN_PREFIX2="${REF_DATA_FN_PRE_PREFIX}_"
RAW_DATA_FN_PRE_PREFIX='CCTM_V5g_Linux2_x86_64intel'
RAW_DATA_FN_PREFIX1="${RAW_DATA_FN_PRE_PREFIX}."
RAW_DATA_FN_PREFIX2="${RAW_DATA_FN_PRE_PREFIX}_"

# NCL `load`s
NCL_FUNCS_DIR="${HOME}/code/regridding/regrid_utils"
export SUMMARIZE_FUNCS_FP="${NCL_FUNCS_DIR}/summarize.ncl"
# which itself requires
export STRING_FUNCS_FP="${NCL_FUNCS_DIR}/string.ncl"
export TIME_FUNCS_FP="${NCL_FUNCS_DIR}/time.ncl"
WORKER_FP="${THIS_DIR}/bias_probe.ncl"

# testing `ncdump -h` heads and tails
# HEAD_PATTERN determined programmatically below
TAIL_PATTERN='// global attributes:'

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

### Call NCL to get bias data and statistics on it (and raw and ref data)
function get_bias_data_and_statistics {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ### Set NCL variables before setting script name ... but note this also uses envvars to tell NCL what to load :-(
  ### -n: don't enumerate values in print()
  # can't make my normal 'for CMD' mechanism work :-(
  # AND can't `tee` from main loop below without duplicating output :-(

#  echo -e "${MESSAGE_PREFIX} ncl -n THIS_FP=\"${WORKER_FP}\" BIAS_DATA_FP=\"${BIAS_DATA_FP}\" RAW_DATA_FP=\"${RAW_DATA_FP}\" REF_DATA_FP=\"${REF_DATA_FP}\" "LOG_FP=\"${LOG_FP}\"" ${WORKER_FP}" 2>&1 | tee -a "${LOG_FP}" # debugging
#  ncl -n "THIS_FP=\"${WORKER_FP}\"" "BIAS_DATA_FP=\"${BIAS_DATA_FP}\"" "RAW_DATA_FP=\"${RAW_DATA_FP}\"" "REF_DATA_FP=\"${REF_DATA_FP}\"" "LOG_FP=\"${LOG_FP}\"" ${WORKER_FP} 2>&1 | tee -a "${LOG_FP}"

  echo -e "${MESSAGE_PREFIX} ncl -n THIS_FP=\"${WORKER_FP}\" BIAS_DATA_FP=\"${BIAS_DATA_FP}\" RAW_DATA_FP=\"${RAW_DATA_FP}\" REF_DATA_FP=\"${REF_DATA_FP}\" ${WORKER_FP}" 2>&1 | tee -a "${LOG_FP}" # debugging
  ncl -n "THIS_FP=\"${WORKER_FP}\"" "BIAS_DATA_FP=\"${BIAS_DATA_FP}\"" "RAW_DATA_FP=\"${RAW_DATA_FP}\"" "REF_DATA_FP=\"${REF_DATA_FP}\"" ${WORKER_FP} 2>&1 | tee -a "${LOG_FP}"
} # function get_bias_data_and_statistics

### Drive `ncdump` etc to compare metadata
function compare_metadata {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local RAW_METADATA_HEAD_FP="${RAW_DATA_FP}.head"
  local REF_METADATA_HEAD_FP="${REF_DATA_FP}.head"
  local RAW_METADATA_TAIL_FP="${RAW_DATA_FP}.tail"
  local REF_METADATA_TAIL_FP="${REF_DATA_FP}.tail"

  ### note extreme similarity to following process for ref*
  if [[ ! -r "${RAW_METADATA_HEAD_FP}" && ! -r "${RAW_METADATA_TAIL_FP}" ]] ; then
    ### make them
    make_metadata "${RAW_DATA_FP}" "${RAW_METADATA_HEAD_FP}" "${RAW_METADATA_TAIL_FP}"
  fi
  # but, JIC ...
  if [[ ! -r "${RAW_METADATA_HEAD_FP}" && ! -r "${RAW_METADATA_TAIL_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read raw data 'ncdump' output:" 2>&1 | tee -a "${LOG_FP}"
    for CMD in \
      "ls -al ${RAW_DATA_FP}.{head,tail}" \
    ; do
      echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${LOG_FP}"
      eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
    done
    exit 2
  fi # [[ -r "${RAW_METADATA_HEAD_FP}" && -r "${RAW_METADATA_TAIL_FP}" ]]

  if [[ ! -r "${REF_METADATA_HEAD_FP}" && ! -r "${REF_METADATA_TAIL_FP}" ]] ; then
    ### make them
    make_metadata "${REF_DATA_FP}" "${REF_METADATA_HEAD_FP}" "${REF_METADATA_TAIL_FP}"
  fi
  # but, JIC ...
  if [[ ! -r "${REF_METADATA_HEAD_FP}" && ! -r "${REF_METADATA_TAIL_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read reference data 'ncdump' output:" 2>&1 | tee -a "${LOG_FP}"
    for CMD in \
      "ls -al ${REF_DATA_FP}.{head,tail}" \
    ; do
      echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${LOG_FP}"
      eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
    done
    exit 3
  fi # [[ -r "${REF_METADATA_HEAD_FP}" && -r "${REF_METADATA_TAIL_FP}" ]]

  ### note extreme similarity to tail processing (following)
  if [[ -r "${RAW_METADATA_HEAD_FP}" && -r "${REF_METADATA_HEAD_FP}" ]] ; then
    for CMD in \
      "diff -wB ${REF_METADATA_HEAD_FP} ${RAW_METADATA_HEAD_FP}" \
    ; do
      echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${LOG_FP}"
      eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
    done
    echo 2>&1 | tee -a "${LOG_FP}" # newline
  fi # [[ -r "${RAW_METADATA_HEAD_FP}" && -r "${REF_METADATA_HEAD_FP}" ]]

  ### note extreme similarity to tail processing (preceding)
  if [[ -r "${RAW_METADATA_TAIL_FP}" && -r "${REF_METADATA_TAIL_FP}" ]] ; then
    for CMD in \
      "diff -wB ${REF_METADATA_TAIL_FP} ${RAW_METADATA_TAIL_FP}" \
    ; do
      echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${LOG_FP}"
      eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
    done
    echo 2>&1 | tee -a "${LOG_FP}" # newline
  fi # [[ -r "${RAW_METADATA_TAIL_FP}" && -r "${REF_METADATA_TAIL_FP}" ]]

} # function compare_metadata

function make_metadata {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local DATA_FP="$1"
  local HEAD_FP="$2"
  local TAIL_FP="$3"
  # TODO: test passed args

  local HEAD_START_LINE_N='1'                                 # by definition
  # note `let` fails, and need for internal double quotes
  local HEAD_END_LINE_N="$(ncdump -h ${DATA_FP} | fgrep -n "${HEAD_PATTERN}" | sed -e 's/:.*$//')"
  local HEAD_LEN="$((HEAD_END_LINE_N-HEAD_START_LINE_N))"

  local TAIL_START_LINE_N="$(ncdump -h ${DATA_FP} | fgrep -n "${TAIL_PATTERN}" | sed -e 's/:.*$//')"
  local TAIL_END_LINE_N="$(ncdump -h ${DATA_FP} | wc -l)" # by definition
  local TAIL_LEN="$((TAIL_END_LINE_N-TAIL_START_LINE_N+1))"

# # start debugging
#   echo -e "len(${HEAD_FP}): ${HEAD_END_LINE_N} - ${HEAD_START_LINE_N}==${HEAD_LEN}" 2>&1 | tee -a "${LOG_FP}"
#   echo -e "len(${TAIL_FP}): ${TAIL_END_LINE_N} - ${TAIL_START_LINE_N}==${TAIL_LEN}" 2>&1 | tee -a "${LOG_FP}"
# #   end debugging

  for CMD in \
    "ncdump -h ${DATA_FP} | head -n ${HEAD_LEN} > ${HEAD_FP}" \
    "ncdump -h ${DATA_FP} | tail -n ${TAIL_LEN} > ${TAIL_FP}" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}" 2>&1 | tee -a "${LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
  done

} # function make_metadata

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

### outer loop: over data, filtering non-netCDF files
for REF_DATA_FP in $(find ${REF_DATA_DIR} -type f | grep -ve 'head$\|tail$' | sort) ; do

  MESSAGE_PREFIX="${THIS_FN}::outer loop:"
  ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ ! -r "${REF_DATA_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read reference data='${REF_DATA_FP}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  elif   [[ ! -s "${REF_DATA_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} reference data='${REF_DATA_FP}' has length=0, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 3
  else # reference data should be OK; what about the raw/test data?
    REF_DATA_FN="$(basename ${REF_DATA_FP})"

#    echo -e "\n$ ${MESSAGE_PREFIX} processing '${REF_DATA_FN}'\n" 2>&1 | tee -a "${LOG_FP}" # debugging

#    JUNK="${REF_DATA_FN#*.}"  # everything after the first '.'
#    NETCDF_TAG="${JUNK%.*}" # everything before the first '.' *after* the first '.'
#   nope, they don't all use this pattern :-(
    JUNK="${REF_DATA_FN/${REF_DATA_FN_PREFIX1}/}" # replace that string with nothing
    JUNK="${JUNK/${REF_DATA_FN_PREFIX2}/}"        # ditto
    NETCDF_TAG="${JUNK%.*}" # discard everything after the first '.'
#    echo -e "${MESSAGE_PREFIX} ${REF_DATA_FN} -> NETCDF_TAG='${NETCDF_TAG}'" 2>&1 | tee -a "${LOG_FP}" # debugging

    case "${NETCDF_TAG}" in
      'AEROVIS')  HEAD_PATTERN='float EXT_Mie' ;;
      'B3GTS_S')  HEAD_PATTERN='float OLE(' ;;
      'DUSTEMIS') HEAD_PATTERN='float QSHGR(' ;;
      'SOILOUT')  HEAD_PATTERN='int PULSEDATE(' ;;
      *)          HEAD_PATTERN='float NO(' ;;
    esac

    # get the 'matching' raw data for comparison. Note:
    # * gotta put delimiters (which here can be [_.] :-( around the tag, otherwise 'CONC' also matches 'ACONC'
    # * need for internal double quotes here----------------------------------------v---------------------v
    RAW_DATA_FP="$(find ${RAW_DATA_DIR} -type f | grep -ve 'head$\|tail$' | grep -e "[_.]${NETCDF_TAG}[_.]")"
    if   [[ ! -r "${RAW_DATA_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} cannot read data to test='${RAW_DATA_FP}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 4
    elif   [[ ! -s "${RAW_DATA_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} data to test='${RAW_DATA_FP}' has length=0, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 5
    else # raw/test data should be OK

      RAW_DATA_FN="$(basename ${RAW_DATA_FP})"
#      BIAS_DATA_FN="${RAW_DATA_FN}.bias"
      # NCL is currently picky about write-file extensions :-(
      BIAS_DATA_FN="${RAW_DATA_FN}.bias.nc"
      BIAS_DATA_FP="${BIAS_DATA_DIR}/${BIAS_DATA_FN}"
#      # start debugging
#      echo -e "${REF_DATA_FP} ->" 2>&1 | tee -a "${LOG_FP}"
#      echo -e "${RAW_DATA_FP}"    2>&1 | tee -a "${LOG_FP}"
#      echo -e "${BIAS_DATA_FP}"   2>&1 | tee -a "${LOG_FP}"
#      echo 2>&1 | tee -a "${LOG_FP}" # newline
#      #   end debugging

      ### inner loop: make and compare metadata for file tuple

      MESSAGE_PREFIX="${THIS_FN}::inner loop:"
      ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#        "compare_metadata" \ # uses HEAD_PATTERN above
#        "get_bias_data_and_statistics" \
      for CMD in \
        "compare_metadata" \
        "get_bias_data_and_statistics" \
      ; do
        echo -e "\n$ ${MESSAGE_PREFIX} '${CMD}'\n" 2>&1 | tee -a "${LOG_FP}"
#        eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
#        if [[ ${PIPESTATUS[0]} -ne 0 ]] ; then # test the *pipeline*, not the `tee`
# Above was writing duplicate output to log, both of command line and command output :-(
# Instead gotta pass LOG_FP to NCL? Or just `tee` NCL output? latter
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
          echo -e "${ERROR_PREFIX} '${CMD}' failed or not found, exiting ...\n" 2>&1 | tee -a "${LOG_FP}"
          exit 10
        fi
      done # for CMD

    fi # [[ ! -r "${RAW_DATA_FP}" ]]
  fi # [[ ! -r "${REF_DATA_FP}" ]]

done # for REF_DATA_FP
exit 0 # success!
