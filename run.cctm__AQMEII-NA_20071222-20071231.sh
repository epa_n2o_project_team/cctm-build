#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### bash rewrite of OAQPS' 08ab.decic (locally, run.cctm__AQMEII-NA_20071222-20071231.sh -- note differing extension)
### ----------------------------------------------------------------------

THIS="$0"
THIS_DIR="$(readlink -f $(dirname ${THIS}))" # absolute path
# THIS_FN="$(basename ${THIS})" # no! under PBS, this becomes, e.g., '27600.imaster.amad.gov.SC'
THIS_FN='run.cctm__AQMEII-NA_20071222-20071231.sh'
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### bash-to-csh translation of two lines in original (both uncommented, not sure why)
ulimit -s 100000 # limit stacksize 100000
ulimit -s unlimited # limit stacksize unlimited

if [[ -n "${PBS_JOBID}" ]] ; then
  echo -e "${MESSAGE_PREFIX} Job ID is ${PBS_JOBID}"
  echo -e "${MESSAGE_PREFIX} Queue is ${PBS_O_QUEUE}"
  # Switch to the working directory; by default PBS launches processes from your home directory.
  echo -e "${MESSAGE_PREFIX} Working directory is ${PBS_WORKDIR}"
  pushd "${PBS_WORKDIR}"
fi
echo -e "${MESSAGE_PREFIX} >>>>>> start model run at $(date)"
# set -x # `set echo` in original

# ----------------------------------------------------------------------
# outside-of-MONTH-loop subrun-variant constants
# ----------------------------------------------------------------------

if [[ -z "${N_SUBRUN}" ]] ; then
  echo -e "${ERROR_PREFIX} N_SUBRUN not defined, exiting ..."
  exit 1
else
  echo -e "${MESSAGE_PREFIX} N_SUBRUN='${N_SUBRUN}'"
fi

if [[ -z "${FIRSTDAY}" ]] ; then
  echo -e "${ERROR_PREFIX} FIRSTDAY not defined, exiting ..."
  exit 2
else
  echo -e "${MESSAGE_PREFIX} FIRSTDAY='${FIRSTDAY}'"
fi

if [[ -z "${YEAR}" ]] ; then
  echo -e "${ERROR_PREFIX} YEAR not defined, exiting ..."
  exit 3
else
  echo -e "${MESSAGE_PREFIX} YEAR='${YEAR}'"
fi

if [[ -z "${YEAR2}" ]] ; then
  echo -e "${ERROR_PREFIX} YEAR2 not defined, exiting ..."
  exit 4
else
  echo -e "${MESSAGE_PREFIX} YEAR2='${YEAR2}'"
fi

if [[ -z "${YR}" ]] ; then
  echo -e "${ERROR_PREFIX} YR not defined, exiting ..."
  exit 5
else
  echo -e "${MESSAGE_PREFIX} if YR='${YR}'"
fi

if [[ -z "${LASTDAY}" ]] ; then
  echo -e "${ERROR_PREFIX} LASTDAY not defined, exiting ..."
  exit 6
else
  echo -e "${MESSAGE_PREFIX} LASTDAY='${LASTDAY}'"
fi

if [[ -z "${INITIAL_RUN}" ]] ; then
  echo -e "${ERROR_PREFIX} INITIAL_RUN not defined, exiting ..."
  exit 7
else
  echo -e "${MESSAGE_PREFIX} INITIAL_RUN='${INITIAL_RUN}'"
fi

if [[ -z "${OUTDATA}" ]] ; then
  echo -e "${ERROR_PREFIX} OUTDATA not defined, exiting ..."
  exit 8
else
  echo -e "${MESSAGE_PREFIX} OUTDATA='${OUTDATA}'"
fi

if [[ -z "${EXEC2}" ]] ; then
  echo -e "${ERROR_PREFIX} EXEC2 not defined, exiting ..."
  exit 9
else
  echo -e "${MESSAGE_PREFIX} EXEC2='${EXEC2}'"
fi

# ----------------------------------------------------------------------
# Calculate day/s-of-month arrays hardcoded in old runners
# Note: same logic in all runned. TODO: refactor!
# ----------------------------------------------------------------------

BDOM=() # 0-based-Julian of first day of month, length=12
MDAY=() # days in each month, length=12
SOURCE_FP="${SCRIPTS_DIR}/CCTM_utilities.sh" # for function setup_day_of_month_arrays
source "${SOURCE_FP}"
if [[ $? -ne 0 ]] ; then
  echo -e "${ERROR_PREFIX} '${SOURCE_FP}': failed or not found\n"
  exit 10
fi
setup_day_of_month_arrays # should set BDOM, MDAY

### Test month-related arrays for ${YEAR}:
## 0-based-Julian of first day of each month
if   [[ -z "${BDOM}" ]] ; then
  echo -e "${ERROR_PREFIX} export=BDOM not found, exiting ..."
  exit 11
elif (( ${#BDOM[@]} != 12 )) ; then
  echo -e "${ERROR_PREFIX} export=BDOM does not have size=12, exiting ..."
  exit 12
fi
## days in each month
if   [[ -z "${MDAY}" ]] ; then
  echo -e "${ERROR_PREFIX} export=MDAY not found, exiting ..."
  exit 13
elif (( ${#MDAY[@]} != 12 )) ; then
  echo -e "${ERROR_PREFIX} export=MDAY does not have size=12, exiting ..."
  exit 14
fi

# # start debugging
# # use subshell for IFS
# ( IFS=',' ; echo -e "\tBDOM       =${BDOM[*]}" 2>&1 | tee -a "${RUNNER_LOG_FP}" )
# ( IFS=',' ; echo -e "\tMDAY       =${MDAY[*]}" 2>&1 | tee -a "${RUNNER_LOG_FP}" )
# #   end debugging

# ----------------------------------------------------------------------
# start loop over MONTH, day
# ----------------------------------------------------------------------

for MONTH in 12 ; do  # note range of MONTH varies by runned
  I_MONTH=$(( ${MONTH} - 1 )) # for array indices
  MM="$(printf '%2.2d' ${MONTH})" # zero-padding needed for APPL

  # compute the first Julian day of the month: logic varies with runned
  if (( ${MONTH} == 12 )) ; then
    day=22
  else
    day=1
  fi
  nday=${MDAY[${I_MONTH}]}

  ### day loop == most of month loop
  while (( ${day} <= ${nday} )) ; do
    ### before the executable
    echo # newline, break in the log at top of day loop
    echo -e "${MESSAGE_PREFIX} process ${YEAR} ${MONTH} ${day}"
#    echo -e "${MESSAGE_PREFIX} Day: ${day}"
    STDAT="$(printf '%2.2d' ${day})" # left-pads with zero
#    @ STDATE   = $day + $BDOM[$MONTH] + 8 * 1000 + 2000000 # ??? in 08ab.jan, 08ab.jun21 ???
    STDATE=$(( ${day} + ${BDOM[${I_MONTH}]} + (${YEAR} * 1000) ))
    APPL="${YEAR}${MM}${STDAT}" # YYYYMMDD
    export YMD="${APPL}" # because CCTM wants it, that's why
    EMISDATE="${APPL}"
#    echo -e "${MESSAGE_PREFIX} Start Date: ${STDAT}"

    yest="$(printf '%2.2d' $(( ${day} - 1 )))"
    if [[ ${yest} == "00" ]] ; then
      if (( ${MONTH} == 1 )) ; then
        MONTH0=12
      else
        MONTH0=$(( ${MONTH} - 1 ))
        I_MONTH0=$(( ${MONTH0} - 1 ))
        yest=${MDAY[${I_MONTH0}]}
      fi
    else
      MONTH0=${MONTH}
    fi

    MONTH0="$(printf '%2.2d' ${MONTH0})"
    yesterday="${YEAR}${MONTH0}${yest}"

    echo -e "${MESSAGE_PREFIX} APPL='${APPL}'"
    # same conditional in 08ab.jan, diff 08ab.jun21
    if (( ${MONTH} == 12 )) ; then
      OUTDIR="${OUTDATA}/${MM}_ic"
    else
      OUTDIR="${OUTDATA}/${MM}"
    fi
    FLOOR_DIR="${OUTDIR}" # floor files normally go in BASE -> SCRIPTS_DIR ???
    echo -e "${MESSAGE_PREFIX} OUTDIR='${OUTDIR}'"
    if [[ ! -d "${OUTDIR}" ]] ; then
      mkdir -p "${OUTDIR}"
    fi

    SOURCE_FP="${SCRIPTS_DIR}/set_pt3demis_2008.q.sh"
    source "${SOURCE_FP}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${SOURCE_FP}': failed or not found\n"
      exit 15
    fi
#    source "${SCRIPTS_DIR}/day_pt3demis_2008.q.sh" # comment in original
    # restore logging. TODO: convert set_pt3demis_2008.q.sh into a function!
    THIS_FN='run.cctm__AQMEII-NA_20071222-20071231.sh'
    MESSAGE_PREFIX="${THIS_FN}:"
    ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

    # TODO: COMPUTE THESE DATES/DATESTRINGS! literals in original :-(
    ### set IC BC files
    if [[ "${APPL}" == "${FIRSTDAY}" ]] ; then
#      set GC_ICpath = /asm1/cyx/amber_work/EPA_GEOS-Chem_ICBC/ICBCSTD52_MER3_2006_24L_US12_new
#      set GC_ICpath = /asm/hnf/amber_work/data/2008/ICBC/icon/2008ck_05b_N1c_inline
      GC_ICpath="${GC_ICpath_0}"
      # TODO: ICFILE="${GC_ICpath}/ICON_CB05AE5_US12_${FIRSTDAY_JJJ}"
      ICFILE="${GC_ICpath}/ICON_CB05AE5_US12_2007356" # 08ab.jan same, 08ab.jun21==2008173
    elif [[ "${APPL}" == 20080101 ]] ; then
      GC_ICpath="${OUTDATA}/12_ic"
      ICFILE="${GC_ICpath}/${EXEC2}.CGRID.20071231"
    elif [[ "${APPL}" == 20080201 ]] ; then
      GC_ICpath="${OUTDATA}/01"
      ICFILE="${GC_ICpath}/${EXEC2}.CGRID.20080131"
    elif [[ "${APPL}" == 20080301 ]] ; then
      GC_ICpath="${OUTDATA}/02"
      ICFILE="${GC_ICpath}/${EXEC2}.CGRID.20080229" # danger! leap year dependency
    elif [[ "${APPL}" == 20080401 ]] ; then
      GC_ICpath="${OUTDATA}/03_ic" # ...03 in 08ab.jan: error here?
      ICFILE="${GC_ICpath}/${EXEC2}.CGRID.20080331"
    elif [[ "${APPL}" == 20080501 ]] ; then
      GC_ICpath="${OUTDATA}/04"
      ICFILE="${GC_ICpath}/${EXEC2}.CGRID.20080430"
    elif [[ "${APPL}" == 20080601 ]] ; then
      GC_ICpath="${OUTDATA}/05"
      ICFILE="${GC_ICpath}/${EXEC2}.CGRID.20080531"
    elif [[ "${APPL}" == 20080701 ]] ; then
      GC_ICpath="${OUTDATA}/06_ic"
      ICFILE="${GC_ICpath}/${EXEC2}.CGRID.20080630"
    elif [[ "${APPL}" == 20080801 ]] ; then
      GC_ICpath="${OUTDATA}/07"
      ICFILE="${GC_ICpath}/${EXEC2}.CGRID.20080731"
    elif [[ "${APPL}" == 20080901 ]] ; then
      GC_ICpath="${OUTDATA}/08"
      ICFILE="${GC_ICpath}/${EXEC2}.CGRID.20080831"
    elif [[ "${APPL}" == 20081001 ]] ; then
      GC_ICpath="${OUTDATA}/09_ic" # ...09_ic in 08ab.decic, 08ab.jan: error here?
      ICFILE="${GC_ICpath}/${EXEC2}.CGRID.20080930"
    elif [[ "${APPL}" == 20081101 ]] ; then
      GC_ICpath="${OUTDATA}/10"
      ICFILE="${GC_ICpath}/${EXEC2}.CGRID.20081031"
    elif [[ "${APPL}" == 20081201 ]] ; then
      GC_ICpath="${OUTDATA}/11"
      ICFILE="${GC_ICpath}/${EXEC2}.CGRID.20081130"
    else
      GC_ICpath="${OUTDIR}"
      ICFILE="${OUTDIR}/${EXEC2}.CGRID.${yesterday}"
    fi

    BCFILE="BCON_CB05AE5_US12_${STDATE}"

    #> set additional parallel log file name extension
    export CTM_APPL="${CFG}__${APPL}" # can't move to runner: APPL set in MONTH/DAY loop

    #> set floor file (neg concs)
    export FLOOR_FILE="${FLOOR_DIR}/FLOOR__${CTM_APPL}"

#    PE_LOG_FILE="${BASE}/CTM_LOG_*.${CTM_APPL}"
    # note filename appears to reflect knowledge of CMAQ/CCTM internals
    PE_LOG_DIR="${RUNNER_LOG_DIR}"
    logs="$(find "${PE_LOG_DIR}" -name "CTM_LOG_*.${CTM_APPL}" | wc -l)"
    if (( ${logs} > 0 )) ; then 
      echo -e "${ERROR_PREFIX} cannot overwrite existing PE log files"
      exit 16
    fi

    ### SET output files

    CONCfile="${EXEC2}.CONC.${APPL}"      # CTM_CONC_1
    ACONCfile="${EXEC2}.ACONC.${APPL}"    # CTM_ACONC_1
    CGRIDfile="${EXEC2}.CGRID.${APPL}"    # CTM_CGRID_1
    DD1file="${EXEC2}.DRYDEP.${APPL}"     # CTM_DRY_DEP_1
    DV1file="${EXEC2}.DEPV.${APPL}"       # CTM_DEPV_DIAG
    PT1file="${EXEC2}.PT3D.${APPL}"       # CTM_PT3D_DIAG
    WD1file="${EXEC2}.WETDEP1.${APPL}"    # CTM_WET_DEP_1
    WD2file="${EXEC2}.WETDEP2.${APPL}"    # CTM_WET_DEP_2
    AV1file="${EXEC2}.AEROVIS.${APPL}"    # CTM_VIS_1
    AD1file="${EXEC2}.AERODIAM.${APPL}"   # CTM_DIAM_1
    SSEfile="${EXEC2}.SSEMIS.${APPL}"     # CTM_SSEMIS_1
    PA1file="${EXEC2}.PA_1.${APPL}"       # CTM_IPR_1
    PA2file="${EXEC2}.PA_2.${APPL}"       # CTM_IPR_2
    PA3file="${EXEC2}.PA_3.${APPL}"       # CTM_IPR_3
    IRR1file="${EXEC2}.IRR_1.${APPL}"     # CTM_IRR_1
    IRR2file="${EXEC2}.IRR_2.${APPL}"     # CTM_IRR_2
    IRR3file="${EXEC2}.IRR_3.${APPL}"     # CTM_IRR_3

    SOURCE_FP="${SCRIPTS_DIR}/outck.q.sh"
    source "${SOURCE_FP}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${SOURCE_FP}': failed or not found\n"
      exit 17
    fi
    # restore logging. TODO: convert outck.q.sh into a function!
    THIS_FN='run.cctm__AQMEII-NA_20071222-20071231.sh'
    MESSAGE_PREFIX="${THIS_FN}:"
    ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

    if [[ -z "${biogon}" ]] ; then
      echo -e "${ERROR_PREFIX} 'biogon' not defined or exported, exiting ..."
      exit 18
    fi
    if (( ${biogon} > 0 )) ; then
      if [[ "${B3GTS_DIAG}" == 'Y' || "${B3GTS_DIAG}" == 'T' ]] ; then
        export B3GTS_S="${OUTDIR}/${EXEC2}.B3GTS_S.${APPL}"
      fi
      export SOILINP="${OUTDIR}/${EXEC2}.SOILINP.${yesterday}"  # Biogenic NO soil input file
      export SOILOUT="${OUTDIR}/${EXEC2}.SOILOUT.${APPL}"       # Biogenic NO soil output file
    fi

    ### SET input files
    EXTN="${YR}${MM}${STDAT}"

    if [[ -z "${pt3don}" ]] ; then
      echo -e "${ERROR_PREFIX} 'pt3don' not defined or exported, exiting ..."
      exit 19
    fi
    if (( ${pt3don} > 0 )) ; then
      EMISfile="emis_mole_all_${EMISDATE}_12US1_cmaq_cb05_F40_2008ab.ag_v2.ncf"
#      source ${SRC}/day_pt3demis_2020ce_200206.q
#      source day_pt3demis.q
      SOURCE_FP="${SCRIPTS_DIR}/day_pt3demis_2008.q.sh"
      source "${SOURCE_FP}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${ERROR_PREFIX} '${SOURCE_FP}': failed or not found\n"
        exit 20
      fi
      # restore logging. TODO: convert day_pt3demis_2008.q.sh into a function!
      THIS_FN='run.cctm__AQMEII-NA_20071222-20071231.sh'
      MESSAGE_PREFIX="${THIS_FN}:"
      ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
      export LAYP_STDATE="${STDATE}"
    else
      EMISfile="emis3d.2020ce.12EUS1_148X112.cb05soa.CL2.${EMISDATE}.24.ocean.ncf"
    fi

    export EMIS_1="${EMISpath}/${EMISfile}"
    export INIT_GASC_1="${ICFILE}"
    export BNDY_GASC_1="${GC_BCpath}/${BCFILE}"
    export INIT_AERO_1="${INIT_GASC_1}"
    export BNDY_AERO_1="${BNDY_GASC_1}"
    export INIT_NONR_1="${INIT_GASC_1}"
    export BNDY_NONR_1="${BNDY_GASC_1}"
    export INIT_TRAC_1="${INIT_GASC_1}"
    export BNDY_TRAC_1="${BNDY_GASC_1}"
    export GRID_DOT_2D="${METpath}/GRIDDOT2D_${EXTN}"
    export GRID_CRO_2D="${METpath}/GRIDCRO2D_${EXTN}"
    export GRID_CRO_3D="${METpath}/GRIDCRO3D_${EXTN}"
    export LAYER_FILE="${METpath}/METCRO3D_${EXTN}"
    export MET_CRO_2D="${METpath}/METCRO2D_${EXTN}"
    export MET_CRO_3D="${METpath}/METCRO3D_${EXTN}"
    export MET_DOT_3D="${METpath}/METDOT3D_${EXTN}"
    export MET_BDY_3D="${METpath}/METBDY3D_${EXTN}"

    ### Are we using lightning NOx?
    if [[ -n "${CTM_LTNG_NO}" && \
	  (  "${CTM_LTNG_NO}" == 'Y' || "${CTM_LTNG_NO}" == 'T' ) \
       ]] ; then
      ltngon=1
    else
      ltngon=0
    fi
 
    if (( ${ltngon} > 0 )) ; then
      if   [[ -z "${LTNG_NO_MODE}" ]] ; then
	echo -e "${ERROR_PREFIX} LTNG_NO_MODE not defined, exiting ..."
	exit 21

      elif [[ "${LTNG_NO_MODE}" == 'inline' ]] ; then
        export LTNGNO='InLine'
        LTNGPARAM='Y' # use lightning parameter file?
        if [[ "${LTNGPARAM}" == 'Y' ]] ; then
          LTNGPARM_FILE="${IN_LTpath}/params/LTNG_RATIO.2004.${MONTH}.ioapi"
          if [[ -r "${LTNGPARM_FILE}" ]] ; then
            export LTNGPARM_FILE
          else
            echo -e "${ERROR_PREFIX} cannot read LTNGPARM_FILE='${LTNGPARM_FILE}', exiting ..."
            exit 22
          fi
        fi
        export LTNGDIAG='Y' # use diagnostic file?
        export LTNGOUT="${OUTDIR}/${EXEC}.LTNGDIAG.${CTM_APPL}"
        # if (! -e $LTNGNO) aget -a $IN_LTpath /asm2/MOD3EVAL/LNOx/emisLNOx/2004af/36US1/pnox3d.t$EMISDATE # in original!

      elif [[ "${LTNG_NO_MODE}" == 'offline' ]] ; then
        LTNGNO="${IN_LTpath}/nox_CMAQ-BENCHMARK.35L.${EMISDATE}"
	if [[ -r "${LTNGNO}" ]] ; then
	  export LTNGNO
	else
	  echo -e "${ERROR_PREFIX} cannot read LTNGNO='${LTNGNO}', exiting ..."
	  exit 23
	fi

      else
	echo -e "${ERROR_PREFIX} cannot handle LTNG_NO_MODE='${LTNG_NO_MODE}', exiting ..."
	exit 24
      fi # if   [[ -z "${LTNG_NO_MODE}" ]]
    fi # if (( ${ltngon} > 0 ))

# comments in original
###&& MET Source: /asm/MOD3EVAL/met/MCIP_v3.4beta3/MM5_2002b_24aL/12EUS1/mcip_out &&####
###&& MET Source: /asm/MOD3EVAL/met/MCIP_v3.4beta4/MM5_2008b_24aL/12EUS1/mcip_out &&####

    JVALfile="JTABLE_${STDATE}"
    export XJ_DATA="${JVALpath}/${JVALfile}"

    export CTM_STDATE="${STDATE}"
    export CTM_STTIME="${STTIME}"
    export CTM_RUNLEN="${NSTEPS}"
    export CTM_TSTEP="${TSTEP}"
    export CTM_PROGNAME="${EXEC}"

    ### Run the executable!
    if   [[ -z "${MPIRUN}" ]] ; then
      echo -e "${ERROR_PREFIX} MPIRUN not defined, exiting ..."
      exit 25
    elif [[ ! -x "${MPIRUN}" ]] ; then
      echo -e "${ERROR_PREFIX} cannot execute MPIRUN='${MPIRUN}', exiting ..."
      exit 26
    elif [[ -n "${CCTM_LOG_ONLY}" ]] ; then # i.e., set CCTM_LOG_ONLY to not run CCTM
      echo -e "${MESSAGE_PREFIX} CCTM_LOG_ONLY='${CCTM_LOG_ONLY}', so not running CCTM='${CCTM_EXEC_FP}'"
    elif [[ -z "${N_PROCS}" ]] ; then
      echo -e "${ERROR_PREFIX} n(processors)==N_PROCS not defined, exiting ..."
      exit 27
    elif (( ${N_PROCS} <= 0 )) ; then
      echo -e "${ERROR_PREFIX} n(processors)=='${N_PROCS}', exiting ..."
      exit 28
    elif [[ -z "${CCTM_EXEC_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} path to executable CCTM_EXEC_FP not defined, exiting ..."
      exit 29
    else
      ### <deep breath/> run CCTM!
      # totalview mpirun -a -v -np $N_PROCS $XBASE/$EXEC; exit() # commented in original
      echo -e "${MESSAGE_PREFIX} execute model ${YEAR} ${MONTH} ${day} @ $(date) ..."
#      time ${MPIRUN} -r ssh -np ${N_PROCS} ${CCTM_EXEC_FP} # terrae (in original)
      time "${MPIRUN}" -n "${N_PROCS}" "${CCTM_EXEC_FP}"   # infinity
      if [[ $? -ne 0 ]] ; then
	echo -e "${ERROR_PREFIX} 'mpirun' of '${CCTM_EXEC_FP}' failed, exiting ...\n"
	exit 30
      fi
    fi # if   [[ -z "${MPIRUN}" ]]

    ### after the executable

#    set ICFILE = $S_CGRID # commented in original

    if (( ${biogon} > 0 )) ; then
      export SOILINP="${OUTDIR}/${EXEC2}.SOILINP.${APPL}"  # Biogenic NO soil input file
      mv "${SOILOUT}" "${SOILINP}"
      export INITIAL_RUN='N'       # same in all
    fi

    (( day++ )) # do the next day

  done # while (( ${day} <= ${nday} ))
done # for MONTH in 12

echo -e "${MESSAGE_PREFIX} >>>>>> end model run at $(date)"
exit 0
