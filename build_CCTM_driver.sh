#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Drive build_CCTM.sh to build project=CCTM: i.e., produce a CCTM executable.
### For details see https://bitbucket.org/epa_n2o_project_team/cctm-build/src/HEAD/README.md#markdown-header-setup ; this automates step 2.

### Requires:
### * `git` (main functionality)

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### Get these out of the way before we get to work:
## message-related
THIS="$0"
# THIS_DIR="$(dirname ${THIS})"
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${THIS_FN}: ERROR:"

### Set *temporary* build root dir/folder: exists ? destroy ! make
export M3HOME='/project/inf35w/roche/CMAQ-5.0.1/temp' # on infinity

### Run this to clone needed repositories
TOP_LEVEL_REPO_NAME='CMAQ-build'
REPO_CLONER_DIR="$(readlink -f ${THIS_DIR}/../${TOP_LEVEL_REPO_NAME})"
REPO_CLONER_FN='clone_repos.sh'
REPO_CLONER_FP="${REPO_CLONER_DIR}/${REPO_CLONER_FN}"

### To build executable=`bldmake`: unfortunately can't do before editing config files
BLDMAKE_BUILD_DIR="${M3HOME}/BLDMAKE-build"
BLDMAKE_BUILDER_FN='build_BLDMAKE.sh'
BLDMAKE_BUILDER_FP="${BLDMAKE_BUILD_DIR}/${BLDMAKE_BUILDER_FN}"

### To build CCTM executable: unfortunately can't do before [editing config files, building `bldmake`]
CCTM_BUILD_DIR="${M3HOME}/CCTM-build"
CCTM_BUILDER_FN='build_CCTM.sh'
CCTM_BUILDER_FP="${CCTM_BUILD_DIR}/${CCTM_BUILDER_FN}"

### TODO: define build package name list PKG_NAME_LIST here, rather than (implicitly) in `for` loop below.

### Root of remote repos: must use HTTP inside EPA firewall
REPO_ROOT_URI='https://bitbucket.org/tlroche'

### `git` prefix needed inside EPA firewall: set null if not needed
GIT_PREFIX='env GIT_SSL_NO_VERIFY=true'

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

START="$(date)"
echo -e "START ${THIS_FN}=${START}"
echo # newline

# ----------------------------------------------------------------------
# setup build dir
# ----------------------------------------------------------------------

if   [ -z "${M3HOME}" ] ; then
  echo -e "${ERROR_PREFIX} M3HOME not defined, exiting ..."
  exit 1
elif [ -d "${M3HOME}" ] ; then
  for CMD in \
    "rm -fr ${M3HOME}/" \
  ; do
    echo -e "${MESSAGE_PREFIX} ${CMD}"
    eval "${CMD}"
  done
fi # if   [ -z "${M3HOME}" ]

if [ -d "${M3HOME}" ] ; then
  echo -e "${ERROR_PREFIX} could not remove M3HOME='${M3HOME}', exiting ..."
  exit 2
### REPO_CLONER_FP will make ${M3HOME}/ , and expects to.
# else
#   for CMD in \
#     "mkdir -p ${M3HOME}/" \
#   ; do
#     echo -e "${MESSAGE_PREFIX} ${CMD}"
#     eval "${CMD}"
#   done
# fi

# if [ -d "${M3HOME}" ] ; then
#   for CMD in \
#     "pushd ${M3HOME}" \
#   ; do
#     echo -e "${MESSAGE_PREFIX} ${CMD}"
#     eval "${CMD}"
#   done
# else
#   echo -e "${ERROR_PREFIX} could not create M3HOME='${M3HOME}', exiting ..."
#   exit 3
fi

echo # newline

# ----------------------------------------------------------------------
# clone build packages
# ----------------------------------------------------------------------

# top_level_repo_name="${TOP_LEVEL_REPO_NAME,,}" # not supported on infinity
top_level_repo_name="$(echo -e ${TOP_LEVEL_REPO_NAME} | tr '[:upper:]' '[:lower:]')"
REPO_URI="${REPO_ROOT_URI}/${top_level_repo_name}"
GIT_CLONE_CMD="${GIT_PREFIX} git clone ${REPO_URI}"
## TODO: fail on failed command
for CMD in \
  "${GIT_CLONE_CMD}" \
  "mv ./${top_level_repo_name} ./${TOP_LEVEL_REPO_NAME} " \
  "${REPO_CLONER_FP}" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
done

# ----------------------------------------------------------------------
# checkout
# ----------------------------------------------------------------------

for CMD in \
  "popd" \
  "ls -alh ${M3HOME}" \
; do
  echo -e "${MESSAGE_PREFIX} ${CMD}"
  eval "${CMD}"
done

echo # newline
echo -e "  END ${THIS_FN}=$(date)"
echo -e "START ${THIS_FN}=${START}"
echo # newline

# ----------------------------------------------------------------------
# advise
# ----------------------------------------------------------------------

cat <<EOM
${MESSAGE_PREFIX}: what to do now:

1. Edit config files to, e.g., show M3HOME='${M3HOME}'. For more details, see
   ${REPO_ROOT_URI}/cctm-build/overview#markdown-header-setup

   If just benchmarking, should only need to edit (same content in each pair)
*  ${M3HOME}/CMAQ-build/uber.config.cmaq.csh
*  ${M3HOME}/CMAQ-build/uber.config.cmaq.sh

   More extensive build changes may require edits to

*  ${M3HOME}/CCTM-build/CCTM_utilities.sh
*  ${M3HOME}/CMAQ-build/config.cmaq.sh
*  ${M3HOME}/CMAQ-build/config.cmaq
*  ${BLDMAKE_BUILDER_FP}
*  ${CCTM_BUILDER_FP}

2. Run '${BLDMAKE_BUILDER_FP}' to build 'bldmake'

3. Run '${CCTM_BUILDER_FP}' to build CCTM executable.

EOM

exit 0 # success!
