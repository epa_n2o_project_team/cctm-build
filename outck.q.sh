#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### /work/romo/2008cdc/CMAQv4.7.1/2008ab_08c_N5ao_inline/12US1/job/outck.q was a csh script,
### written by US EPA OAQPS for their CDC/PHASE-2008 run of CMAQ:
### for details on that run, see
### https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/EPA_CDC_PHASE_run_for_2008
### Mechanically translated to bash by Tom Roche.

### From head of outck.q:
# /amber:/home/yoj/src/emis/plrise/outck.q
# RCS file, release, date & time of last delta, author, state, [and locker]
# $Header: /project/work/rep/SCRIPTS/src/cctm/in_out.q,v 1.4 2005/09/09 13:53:15 sjr Exp $ 
# what(1) key, module and SID; SCCS file; date and time of last delta:
# %W% %P% %G% %U%

THIS="$0"
THIS_DIR="$(readlink -f $(dirname ${THIS}))" # absolute path
# THIS_FN="$(basename ${THIS})" # no! under PBS, this becomes, e.g., '27600.imaster.amad.gov.SC'
THIS_FN='outck.q.sh'
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

# action if the output files already exist ...
   export CTM_CONC_1="${OUTDIR}/${CONCfile} -v"
export CTM_PT3D_DIAG="${OUTDIR}/${PT1file} -v"
export CTM_WET_DEP_1="${OUTDIR}/${WD1file} -v"
export CTM_WET_DEP_2="${OUTDIR}/${WD2file} -v"
    export CTM_VIS_1="${OUTDIR}/${AV1file} -v"
   export CTM_DIAM_1="${OUTDIR}/${AD1file} -v"
 export CTM_SSEMIS_1="${OUTDIR}/${SSEfile} -v"
    export CTM_IPR_1="${OUTDIR}/${PA1file} -v"
    export CTM_IPR_2="${OUTDIR}/${PA2file} -v"
    export CTM_IPR_3="${OUTDIR}/${PA3file} -v"
    export CTM_IRR_1="${OUTDIR}/${IRR1file} -v"
    export CTM_IRR_2="${OUTDIR}/${IRR2file} -v"
    export CTM_IRR_3="${OUTDIR}/${IRR3file} -v"
      export S_CGRID="${OUTDIR}/${CGRIDfile}"
#              $S_CGRID\#

flist=( "${CTM_CONC_1}" \
        "${A_CONC_1}" \
        "${CTM_DRY_DEP_1}" \
        "${CTM_DEPV_DIAG}" \
        "${CTM_PT3D_DIAG}" \
        "${CTM_WET_DEP_1}" \
        "${CTM_WET_DEP_2}" \
        "${CTM_VIS_1}" \
        "${CTM_DIAM_1}" \
        "${CTM_SSEMIS_1}" \
        "${CTM_IPR_1}" \
        "${CTM_IPR_2}" \
        "${CTM_IPR_3}" \
        "${CTM_IRR_1}" \
        "${CTM_IRR_2}" \
        "${CTM_IRR_3}" )

unalias rm 
for file in "${flist[@]}" ; do
  if [[ "${file}" != '-v' ]] ; then
    if [[ -e "${file}" ]] ; then
       echo -e "${MESSAGE_PREFIX} ${file} already exists"
       if [[ "${DISP}" == 'delete' ]] ; then
         echo -e "${MESSAGE_PREFIX} ${file} being deleted"
         rm "${file}"
       elif [[ "${DISP}" == 'update' ]] ; then
         echo -e "${MESSAGE_PREFIX} ${file} being updated"
       else
         echo -e "${MESSAGE_PREFIX} DISP != ('delete', 'update'), exiting ..."
         exit 1
       fi
     fi
  fi
done
