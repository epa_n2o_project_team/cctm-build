#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### This "runner" will overload a few key items before calling run.cctm_*.sh (the "runned," pronounced with 2 syllables)
### to run AQMEII-NA_2008 in 3 parts, à la the OAQPS run--see
### https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/EPA_CDC_PHASE_run_for_2008#!cdcphase-2008-run
### TODO: check output (sleep if PBS-ing)

### Requirements:
### * new enough `bash` to do arithmetic, string manipulation, arrays
### * `basename`
### * `chmod`
### * `date`
### * `dirname`
### * `printf`
### * `readlink`
### * `sed`
### * `whoami`

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### These are just the "top-level" constants;
### *even more* constants are set below in `function setup_constants`

THIS="$0"
# THIS_DIR="$(dirname ${THIS})"
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### Run parallel under PBS on infinity.
export RUN_PBS='yes!' # TODO: use my "real" bash booleans
## PBS node/PPN allocation: allow overload from commandline (for testing only!)
if [[ -z "${N_NODES}" ]] ; then
  export N_NODES=24         # HPCC total=57
fi
export N_PROCS_PER_NODE=8 # fixed on HPCC (some with ppn=4, but those are for I/O)
export N_PROCS=$(( ${N_NODES} * ${N_PROCS_PER_NODE} )) # needed for `mpirun -n`
## IOAPI processor grid
NPROW='8'   # one row due to domain domination by westerlies, per JOB?
# NPCOL=$(( ${N_PROCS} - ${NPROW} )) # IOAPI vomits if 'NPROCS is not equal to NPCOL*NPROW'
NPCOL=$(( ${N_PROCS} / ${NPROW} )) # but bash only knows integer arithmetic, so ...
NPROCS=$(( ${NPCOL} *  ${NPROW} ))
if (( ${N_PROCS} != ${NPROCS} )) ; then
  echo -e "${ERROR_PREFIX} N_NODES='${N_NODES}'"
  echo -e "${ERROR_PREFIX} N_PROCS_PER_NODE='${N_PROCS_PER_NODE}'"
  echo -e "${ERROR_PREFIX} N_PROCS='${N_PROCS}'"
  echo -e "${ERROR_PREFIX} NPCOL='${NPCOL}'"
  echo -e "${ERROR_PREFIX} NPROW='${NPROW}'"
  echo -e "${ERROR_PREFIX} NPROCS='${NPROCS}'"
  echo -e "${ERROR_PREFIX} incorrect processor math, exiting ..."
  exit 1
fi

### Where to put output? these are just root paths, to be appended below:
LOG_ROOT_DIR='/project/inf38w' # for both runner and subruns (at least for PBS)
## First is for spinup, can be small. Latter need to be larger (but how much?)
OUTPUT_ROOT_DIR_ARR=(\
  "/project/inf17w"  \
  "/project/inf39w"  \
  "/project/inf36w"  \
)
## TODO: check for appropriate/estimated freespace on above paths.
## And don't put output on the following disk, if platform=infinity:
INPUT_ROOT_DIR='/project/inf35w'
GIT_ROOT_DIR="${INPUT_ROOT_DIR}/roche/CMAQ-5.0.1/git/old" # for git repos
## {read from, write to} same disk is not performant on HPCC/infinity (I am told)
## * scripts, configs, and grids all on /project/inf35w
## * CCTM {build, executable}        on /project/inf35w
## * input BC/ICs                    on /project/inf35w
## * input emissions                 on /project/inf35w
## * input J data                    on /project/inf35w
## * input met                       on /project/inf35w
## * input ocean/surf mask file      on /project/inf35w
## * input ag/crop activity files    on /project/inf35w
## * input BELD dust files           on /project/inf35w

### Override if not running under PBS.
if [[ -z "${RUN_PBS}" ]] ; then
    # not running PBS -> only run serial, cannot override:
    NPCOL='1'
    NPROW='1'
    N_PROCS='1'
    export NPCOL_NPROW='1 1'
else
  # differs by run.cctm! # PBS_WALLTIME_LIMIT='10:00:00'  # HH:MM:SS
  PBS_QUEUE_NAME='batch'         # only one available on HPCC/infinity
fi # if [[ -z "${RUN_PBS}" ]]

### output (including logging)
## define output tag/name, write location
CMAQ_VERSION='5.0.1-release'
FAMILY_NAME='AQMEII-NA_2008_N2O'
N_RUN='0' # first AQMEII-NA_2008 run: increase as we add runs
OUTPUT_SUBPATH="${FAMILY_NAME}/${N_RUN}"
OUTPUT_TAG="$(echo -e ${OUTPUT_SUBPATH} | sed -e 's|/|_run_|g')"
if [[ -n "${RUN_PBS}" ]] ; then
  # Note this prefixes the job name for the runned only!
  PBS_JOB_NAME_PREFIX="${OUTPUT_TAG}"
fi

## Vastly preferable to have one output dir, but apparently not available on HPCC/infinity
# export CCTM_OUTPUT_DIR="/project/inf36w/roche/CMAQ-5.0.1/${OUTPUT_SUBPATH}"
# ## ASSERT: if (exists(output dir)) then DON'T whack(output dir)! until exists(archive)!
# if   [[ -z "${CCTM_OUTPUT_DIR}" ]] ; then
#   echo -e "${ERROR_PREFIX} CCTM_OUTPUT_DIR not defined, exiting ..."
#   exit 2
# elif [[ -d "${CCTM_OUTPUT_DIR}" ]] ; then
#   echo -e "${ERROR_PREFIX} output dir='${CCTM_OUTPUT_DIR}' exists--move or delete it! Exiting ..."
#   exit 3
# # else # make it in run.cctm_runner.sh::test_top_constants
# fi

## logging-related
DATETIME_FORMAT='%Y%m%d_%H%M'
DATETIME_STAMP="$(date +${DATETIME_FORMAT})"
RUNNER_LOG_FN="${THIS_FN}__${DATETIME_STAMP}.log"
# RUNNER_LOG_DIR="${THIS_DIR}"
# keep logs with data? unfortunately, can't on HPCC/infinity
# RUNNER_LOG_DIR="${CCTM_OUTPUT_DIR}"
# but also don't want to write to same HPCC host as we're reading from!
# RUNNER_LOG_DIR="${INPUT_ROOT_DIR}/roche/CMAQ-5.0.1/${OUTPUT_SUBPATH}"
export RUNNER_LOG_DIR="${LOG_ROOT_DIR}/roche/${OUTPUT_SUBPATH}" # also for CCTM logs
export RUNNER_LOG_FP="${RUNNER_LOG_DIR}/${RUNNER_LOG_FN}"

## put all logs together? PBS/runned and runner?
if [[ -n "${RUN_PBS}" ]] ; then
  PBS_LOG_DIR="${RUNNER_LOG_DIR}"
fi

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# test top-level constants
# ----------------------------------------------------------------------

function test_top_constants {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ -z "${RUN_PBS}" ]] ; then
    echo -e "${MESSAGE_PREFIX} running directly (RUN_PBS undefined)"
  else # debugging, really
    echo -e "${MESSAGE_PREFIX} will run under PBS (RUN_PBS='${RUN_PBS}')"
  fi

#  ### major question: where to write output?
#  if [[ -z "${CCTM_OUTPUT_DIR}" ]] ; then # allow override by wrappers, commandline
#    echo -e "${ERROR_PREFIX} output root CCTM_OUTPUT_DIR undefined, exiting ..."
#    exit 4
#  fi

  ### major question: where to write log?
  if [[ -z "${RUNNER_LOG_DIR}" ]] ; then # allow override by wrappers, commandline
    echo -e "${ERROR_PREFIX} output root RUNNER_LOG_DIR undefined, exiting ..."
    exit 5
  fi

  ### major question: how is the IOAPI processor grid defined?
  if [[ -z "${NPCOL}" ]] ; then # allow override by wrappers, commandline
    echo -e "${ERROR_PREFIX} IOAPI column processor count NPCOL undefined, exiting ..."
    exit 6
  fi
  if [[ -z "${NPROW}" ]] ; then # allow override by wrappers, commandline
    echo -e "${ERROR_PREFIX} IOAPI row processor count NPROW undefined, exiting ..."
    exit 7
  fi

  ### NOTE: currently cannot write both this and PBS to same log! see
  ### http://www.supercluster.org/pipermail/torqueusers/2013-November/016421.html
  if   [[ -z "${RUNNER_LOG_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} log path RUNNER_LOG_FP undefined, exiting ..."
    exit 8
  elif [[ -r "${RUNNER_LOG_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} log='${RUNNER_LOG_FP}' exists. Is another job running? Exiting JIC ..."
    exit 9
  else # debugging, really. DON'T tee yet! could be writing log to output dir
    echo -e "${MESSAGE_PREFIX} will write to log='${RUNNER_LOG_FP}'"
    if [[ -n "${RUN_PBS}" ]] ; then
      echo -e "${MESSAGE_PREFIX} note PBS will write to separate log!"
    fi
  fi

  if   [[ -n "${RUN_PBS}" ]] ; then
    if [[ -z "${PBS_JOB_NAME_PREFIX}" ]] ; then # allow override by wrappers, commandline
      echo -e "${ERROR_PREFIX} name of top-level job PBS_JOB_NAME_PREFIX undefined, exiting ..."
      exit 10
    fi
  fi

} # function test_top_constants

# ----------------------------------------------------------------------
# setup below-top-level constants. user: edit here!
# ----------------------------------------------------------------------

function setup_constants {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ### Time to setup the output dir for logging.
  for CMD in \
    "mkdir -p ${RUNNER_LOG_DIR}/" \
  ; do
    echo -e "${MESSAGE_PREFIX} '${CMD}'"
    eval "${CMD}"
  done

  if   [[ ! -d "${RUNNER_LOG_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot make log dir/folder='${RUNNER_LOG_DIR}', exiting ..." # 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 11
#  elif [[ ! -d "${RUNNER_LOG_DIR}" ]] ; then
#    echo -e "${ERROR_PREFIX} cannot make log dir/folder='${RUNNER_LOG_DIR}', exiting ..." # 2>&1 | tee -a "${RUNNER_LOG_FP}"
#    exit 12
  else
    ### ASSERT: can write runner log from here on out
    START="$(date)"
    echo -e "${MESSAGE_PREFIX} START=${START}" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    echo 2>&1 | tee -a "${RUNNER_LOG_FP}" # newline
  fi # if   [ ! -d "${RUNNER_LOG_DIR}" ]]

  ### Non-loop, run-invariant constants:
  ## cores/processors and nodes
  export NPCOL_NPROW="${NPCOL} ${NPROW}" # used by IOAPI
  if [[ -n "${RUN_PBS}" ]] ; then
    ## for PBS: host decomposition
    # CMAQ-5.0.1 benchmark: given met domination by westerlies, hope PBS allocates 1 row/node!
    if [[ -z "${N_NODES}" ]] ; then # allow override by wrappers, commandline
      N_NODES="${NPROW}"
    fi
    if [[ -z "${N_PROCS_PER_NODE}" ]] ; then # allow override by wrappers, commandline
      N_PROCS_PER_NODE="${NPCOL}"
    fi
    (( N_x_PPN=${N_NODES} * ${N_PROCS_PER_NODE} )) # bash arithmetic expansion syntax
    if (( ${N_x_PPN} != ${N_PROCS} )); then
      echo -e "${ERROR_PREFIX} N_x_PPN='${N_x_PPN}' != N_PROCS='${N_PROCS}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
      exit 13
    fi

    ## more PBS-related vars we can default (otherwise set in following loop)
    export PBS_EXPORT_ENVIRONMENT='-V'    # blank to not export caller's envvars
    PBS_JOIN_OPTION='oe'           # log join: put both stdout and stderr into the same output/log file]
    PBS_MAIL_OPTION='n'            # can't send email outside the firewall from HPCC/infinity? or any EPA host?

  else
    # not running PBS -> only run serial, cannot override:
    NPCOL='1'
    NPROW='1'
    N_PROCS='1'
    export NPCOL_NPROW='1 1'
  fi # if [[ -n "${RUN_PBS}" ]]

  ### More non-loop, run-invariant constants
  ## We will drive a sequence of runneds (aka run.cctm), which we'll pass to the next level down.
  CCTM_RUNNED_DIR="${THIS_DIR}" # used in function=run_direct
  CCTM_RUNNED_FN_PREFIX='run.cctm__AQMEII-NA'
  if [[ -n "${RUN_PBS}" ]] ; then
    # not a normal PBS envvar: I added PBS_WORKDIR to make the damn thing go where I want (see logfile or commit message)
#    export PBS_WORKDIR="${CCTM_RUNNED_DIR}"
    export PBS_WORKDIR="${RUNNER_LOG_DIR}" # since IOAPI wants to write logs there
  fi

# ----------------------------------------------------------------------
# Constants that DON'T change between OAQPS subruns.
# Constants that DO change between OAQPS subruns go in either
# * function=run (in this file) or function it calls
# * runned script (not in this file)
# ----------------------------------------------------------------------

  ### Copy/mod constants from OAQPS 08ab* == ./run.cctm__AQMEII-NA_*.csh. TODO: refactor!
  ### See documentation for constants:
  ### * input paths: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/EPA_CDC_PHASE_run_for_2008#!inputs
  ### * output paths: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/EPA_CDC_PHASE_run_for_2008#!outputs
  ### * other config vars: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/EPA_CDC_PHASE_run_for_2008#!configuration-variables

  TYPE='inline' # used in conditional below
  ### setup the executable directories
#  BASE="${GIT_ROOT_DIR}/CCTM-build"
  # dir/folder for scripts (and logs and floor files in original): separate and be more mnemonic:
  export SCRIPTS_DIR="${GIT_ROOT_DIR}/CCTM-build" 
  # dir/folder for my config.*
  CONFIGS_DIR="${GIT_ROOT_DIR}/CMAQ-build" 
  CONFIG_FN='config.cmaq.sh'
  CONFIG_FP="${CONFIGS_DIR}/${CONFIG_FN}"
  UBER_CONFIG_FN='uber.config.cmaq.sh'
  UBER_CONFIG_FP="${CONFIGS_DIR}/${UBER_CONFIG_FN}"

  CFG="${FAMILY_NAME}__${N_RUN}" # only used in following
#  export CTM_APPL="${CFG}__${APPL}" # can't define here: APPL set in runned MONTH/DAY loops
#  export EXEC="CCTM_${CFG}" # filename for CCTM executable: be more mnemonic below
#  EXEC2="${EPI3}" prefixes output filenames: be more mnemonic
  export OUTPUT_FN_PREFIX="CCTM_${CMAQ_VERSION}_${FAMILY_NAME}"

#  XBASE="${GIT_ROOT_DIR}/BLD_CCTM_CMAQv5_0_1" # dir/folder for CCTM executable: be more mnemonic
  CCTM_EXEC_DIR="${GIT_ROOT_DIR}/BLD_CCTM_CMAQv5_0_1"
  CCTM_EXEC_FN='CCTM_CMAQv5_0_1_Linux2_x86_64intel' # TODO: change CCTM-build to use CMAQ-version
  export CCTM_EXEC_FP="${CCTM_EXEC_DIR}/${CCTM_EXEC_FN}"

#  SRC="${BASE}" # only used as dirname for *.q: just use SCRIPTS_DIR
  export GRIDS_DIR="${GIT_ROOT_DIR}/CMAQ-grids/AQMEII-NA"

  ### Set common environmental variables
  export STTIME='000000'
  export NSTEPS='240000'
  export CTM_TSTEP='010000'

  export KZMIN='Y' # use Min Kz option in edyintb [Y], otherwise revert to Kz0UT
  export GRIDDESC="${GRIDS_DIR}/GRIDDESC" # horizontal grid definition file
  export GRID_NAME='12US1'                # horizontal grid definition tag

  #export AVG_CONC_SPCS "O3 NO CO NO2, ASO4I,ASO4J NH3" # commented in original
  export AVG_CONC_SPCS='ALL' # species for integral average conc

  export ACONC_BLEV_ELEV=' 1 1' # layer range for integral average conc 

  # set to 300 to avoid "downburst divergence advection failures"
  export CTM_MAXSYNC='300' # max sync time step (sec) [720]
  export CTM_MINSYNC='60'
  export CTM_SYMPROC='N'
  export CTM_CKSUM='Y'

  export CTM_PHOTDIAG='Y' # use PHOTOlysis diagnostic file?

  export CLD_DIAG='Y' # use cloud diagnostic file?

  export CTM_WVEL='Y' # save derived vertical velocity component to conc file?

  export CTM_AERDIAG='Y' # use aerosol diagnostic file?

  export CTM_DEPV_FILE='Y' # use diagnostic file for deposition velocities?

#  setenv CTM_EMLAYS 34  # Number of emission layers # commented in original

#  echo -e "${MESSAGE_PREFIX} TYPE='${TYPE}'" 2>&1 | tee -a "${RUNNER_LOG_FP}" # debugging
  if   [[ "${TYPE}" == 'base' ]] ; then
    export CTM_BIOGEMIS='N'
    export CTM_PT3DEMIS='N'
  elif [[ "${TYPE}" == 'inline' ]] ; then
    export CTM_BIOGEMIS='N' # Y -> use inline biogenic emissions
    export CTM_PT3DEMIS='Y' # Y -> use inline plume rise emissions
  else
    echo -e "${ERROR_PREFIX} cannot handle TYPE='${TYPE}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 14
  fi

  export IOAPI_LOG_WRITE='F' # turn off excess WRITE3 logging?

  export FL_ERR_STOP='F' # stop on inconsistent input file?

  export PROMPTFLAG='F' # use I/O-API PROMPT*FILE interactive mode?

  ### Set input directories: see
  ### https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/EPA_CDC_PHASE_run_for_2008#!inputs
  export METpath="${INPUT_ROOT_DIR}/roche/met/MCIP_v3.6/WRF_2008_24aL/12US1/mcip_out"
  export JVALpath="${INPUT_ROOT_DIR}/roche/JTABLE"
  CROP_ACTIVITY_DIR="${INPUT_ROOT_DIR}/roche/crop"
  DUST_DIR="${INPUT_ROOT_DIR}/roche/dust"
  export IN_LTpath="${INPUT_ROOT_DIR}/roche/lightning" # used in runned: daily files
  OMI_DIR="${INPUT_ROOT_DIR}/roche/phot"
  export OCEAN_1="${INPUT_ROOT_DIR}/roche/ocean/12US1_surf.ncf" # ocean/surf mask file

  ### start constants from CMAQ-5.0.1 benchmark (using same values where feasible)

  ### this one added by me to control tracer lookup
  export CTM_USE_TRACERS='N' # use tracer species? lookup tracer namelist?

  export CTM_WB_DUST='N' # use windblown dust? 'Y' in benchmark, but off as of 24 Jan 2014: no DUST_LU_* for my domain
  if [[ -n "${CTM_WB_DUST}" && \
        (  "${CTM_WB_DUST}" == 'Y' || "${CTM_WB_DUST}" == 'T' ) \
     ]] ; then
    # evaluate these (they're ignored unless CTM_WB_DUST is explicitly set)
    export CTM_ERODE_AGLAND='Y' # use agricultural activity for windblown dust?
    export CTM_DUSTEM_DIAG='Y'  # use windblown dust emissions diagnostic file?
  fi

  export CTM_LTNG_NO='N' # turn on lightning NOx? no, as of 24 Jan 2014: no offline for my domain, inline sounds slow
  ### logic added by me to replace CMAQ-5.0.1 benchmark inline/offline kludge (in runned)
  if [[ -n "${CTM_LTNG_NO}" && \
        (  "${CTM_LTNG_NO}" == 'Y' || "${CTM_LTNG_NO}" == 'T' ) \
     ]] ; then
    export LTNG_NO_MODE='offline' # or 'inline'
  fi

  export CTM_ILDEPV='Y' # use inline deposition velocities? *default* is [ Y|T ]
  if [[ -n "${CTM_ILDEPV}" && \
        (  "${CTM_ILDEPV}" == 'N' || "${CTM_ILDEPV}" == 'F' ) \
     ]] ; then
    # evaluate these (unless CTM_ILDEPV is explicitly cleared)
    export CTM_MOSAIC='N'   # use landuse specific deposition velocities?
    export CTM_ABFLUX='N'   # use NH3 bi-directional flux for inline deposition velocities?
    export CTM_HGBIDI='N'   # use Hg bi-directional flux for inline deposition velocities?
    export CTM_SFC_HONO='Y' # do surface HONO interaction?
  fi

  export IOAPI_OFFSET_64='NO' # guess 1 char ain't enough for IOAPI :-)
  export EXECUTION_ID="${CCTM_EXEC_FN}" # model execution id: why? (was $EXEC)

  ## define new inputs:
  # * following depend on choice of chemical mechanism
  CHEM_MECH_NAME='cb05tucl_ae6_aq'        # was MECH in CMAQ-5.0.1 benchmark
  CHEM_MECHS_REPO="${GIT_ROOT_DIR}/MECHS" # each repo has multiple mechanisms
  CHEM_MECH_DIR="${CHEM_MECHS_REPO}/${CHEM_MECH_NAME}"

  # * species definition namelists (envvar lowercase because CCTM wants it thus!
  AE_NML_FN="AE_${CHEM_MECH_NAME}.nml"     # aerosol namelist filename
  AE_NML_FP="${CHEM_MECH_DIR}/${AE_NML_FN}"
  if [[ -r "${AE_NML_FP}" ]] ; then
    export ae_matrix_nml="${AE_NML_FP}"
  else
    echo -e "${ERROR_PREFIX} cannot read aerosol namelist='${AE_NML_FP}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 15
  fi

  GC_NML_FN="GC_${CHEM_MECH_NAME}.nml"     # gas-phase-chemistry namelist filename
  GC_NML_FP="${CHEM_MECH_DIR}/${GC_NML_FN}"
  if [[ -r "${GC_NML_FP}" ]] ; then
    export gc_matrix_nml="${GC_NML_FP}"
  else
    echo -e "${ERROR_PREFIX} cannot read gas-phase namelist='${GC_NML_FP}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 16
  fi

  NR_NML_FN="NR_${CHEM_MECH_NAME}_N2O.nml" # non-reactive namelist filename--this one's ours!
  NR_NML_FP="${CHEM_MECH_DIR}/${NR_NML_FN}"
  if [[ -r "${NR_NML_FP}" ]] ; then
    export nr_matrix_nml="${NR_NML_FP}"
  else
    echo -e "${ERROR_PREFIX} cannot read non-reactive namelist='${NR_NML_FP}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 17
  fi

  if [[ -n "${CTM_USE_TRACERS}" && \
        (  "${CTM_USE_TRACERS}" == 'Y' || "${CTM_USE_TRACERS}" == 'T' ) \
     ]] ; then

    TR_NML_FN="TR_${CHEM_MECH_NAME}.nml"     # tracer namelist filename
    TR_NML_FP="${CHEM_MECH_DIR}/${TR_NML_FN}"
    if [[ -r "${TR_NML_FP}" ]] ; then
      export tr_matrix_nml="${TR_NML_FP}"
    else
      echo -e "${ERROR_PREFIX} cannot read tracer namelist='${TR_NML_FP}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
      exit 18
    fi
  else
    # unset it? no, ...
    # tr_matrix_nml=''        # CCTM insists on looking up .../tr_matrix_nml
    # export tr_matrix_nml='' # CCTM insists on looking up .../ (a dir!)
    # ... ya gotta give CCTM "something for nothing": a namelist of no tracers
    export tr_matrix_nml="${CHEM_MECHS_REPO}/trac0/Species_Table_TR_0.nml"
  fi

  # * photolysis
  PHOTO_FN="CSQY_DATA_${CHEM_MECH_NAME}"
  PHOTO_FP="${CHEM_MECH_DIR}/${PHOTO_FN}"
  if [[ -r "${PHOTO_FP}" ]] ; then
    export CSQY_DATA="${PHOTO_FP}"         # uppercase envvar in CMAQ-5.0.1 benchmark
  else
    echo -e "${ERROR_PREFIX} cannot read photolysis datafile='${PHOTO_FP}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 19
  fi

  ###   end constants from CMAQ-5.0.1 benchmark (using same values)

  ## Will we do inline biogenic emissions processing?
  ## Note: biogon is used by runned
  if [[ -n "${CTM_BIOGEMIS}" ]] ; then
    if [[ "${CTM_BIOGEMIS}" == 'Y' || "${CTM_BIOGEMIS}" == 'T'  ]] ; then
      export biogon=1
    else
      export biogon=0
    fi
  else         # $CTM_BIOGEMIS is not defined => $CTM_BIOGEMIS == 'Y'
    export biogon=1
  fi

  ### I don't currently know how to handle inline biogenics.
  if (( ${biogon} > 0 )) ; then
    echo -e "${ERROR_PREFIX} cannot handle inline biogenics, exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 20
#     GSPROpath='/work/dkj/20020101'
#     export GSPRO="${GSPROpath}/gspro_cb05soa_notoxics_cmaq_poc_09nov2008.txt"
#     IN_BEISpath='/work/dkj/data/ORD/A41/smoke_out/2020af/12EUS1/cmaq_cb05_soa/biog'
#     export B3GRD="${IN_BEISpath}/b3grd.3.14.12EUS1.ncf"
#     export BIOG_SPRO='B10C5' # speciation profile to use for biogenics
#     export BIOSW_YN='N'      # use frost date switch?
# #    export SUMMER_YN     N  # Use summer normalized emissions? # commented in original
# #    export PX_VERSION    Y  # MCIP is PX version? [N|F] # commented in original
#      export B3GTS_DIAG='N'   # use BEIS mass emissions diagnostic file?

#      # Biogenic NO soil input file
#      # non-existent or not using SOILINP [N|F]; default uses SOILINP
# #     export SOILINP ${OUTDIR}/${EXEC}"_SOILINP".${YEAR}${MONTH}${IDAY} # commented in original
  fi

  ## pt3don is used in runned
  if [[ -n "${CTM_PT3DEMIS}" ]] ; then
    if [[ "${CTM_PT3DEMIS}" == 'Y' || "${CTM_PT3DEMIS}" == 'T' ]] ; then
      export pt3don=1
    else
      export pt3don=0
    fi
  else # $CTM_PT3DEMIS is not defined => $CTM_PT3DEMIS == 'Y'
    export pt3don=1
  fi

  if (( ${pt3don} > 0 )) ; then
# following commented in original
#    set IN_PTpath  = /work/romo/hdghg/smoke_out/2008cs_hdghg_05b/12EUS1/cmaq_cb05_tx
#    set IN_PTpath  = /work/yoj/data/2006/08/inline
#    Plumerise Sector file source: /asm1/MOD3APP/chesbay/smoke_out/2020cc_070/12EUS1/cmaq_cb05soa/inln
#    set IN_PTpath2 = ${IN_PTpath}/inln
#    set STKCASE = 36km_2020ce.inline
#    set STKCASE1 = 12EUS1_2008ck_05b
#    set STKCASE2 = 12EUS1_2008ck_05b

    export PT3DDIAG='Y' # optional 3d point source emissions diagnostic file [N]

# following commented in original
#    export PT3DFRAC Y  # optional layer fractions diagnostic (play) file(s) [N]
#    set +x # `unset echo` in original
#    export EMIS_DATE ${YEAR}${MONTH}
#    source ${SRC}/set_pt3demis_200208.q
#    source set_pt3demis.q
#    source ${SRC}/set_pt3demis_2020ce_200206.q
#    source ${SRC}/set_pt3demis_2008.q

    # export LAYP_STDATE="${STDATE}" # set in runned MONTH/DAY loops
    export LAYP_STTIME="${STTIME}"
    export LAYP_NSTEPS="${NSTEPS}"
    export EMISpath="${INPUT_ROOT_DIR}/roche/emis/2008N2O/AQMEII-NA_N2O_integration/AQplusN2O"
  else
    ### I don't currently know how to handle !(plume rise).
    echo -e "${ERROR_PREFIX} cannot handle pt3don=='${pt3don}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 21
#     # on EMVL/terrae: no such path on HPCC/infinity
#     EMISpath="/orchid/oaqps/rfs2/smoke_out_v4/${EPI2}/12EUS1/cmaq_cb05_soa"   #3D
  fi # if [[ -n "${CTM_PT3DEMIS}" ]]

  ### Use files=*config.cmaq* to get M3DATA, MPIRUN, etc
  if [[ -r "${UBER_CONFIG_FP}" ]] ; then
    source "${UBER_CONFIG_FP}"
  else
    echo -e "${ERROR_PREFIX} cannot 'source' uber-config='${UBER_CONFIG_FP}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 22
  fi

  if [[ -r "${CONFIG_FP}" ]] ; then
    source "${CONFIG_FP}"
  else
    echo -e "${ERROR_PREFIX} cannot 'source' config='${CONFIG_FP}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 23
  fi

  if [[ -n "${CTM_ERODE_AGLAND}" ]] ; then
    if [[ "${CTM_ERODE_AGLAND}" == 'Y' || "${CTM_ERODE_AGLAND}" == 'T' ]] ; then
      export aglandon=1
    else
      export aglandon=0
    fi
  else # $CTM_ERODE_AGLAND is not defined => $CTM_ERODE_AGLAND == 'N'
    export aglandon=0
  fi

  if (( ${aglandon} > 0 )) ; then
    CROPMAP01="${CROP_ACTIVITY_DIR}/BeginPlanting_12km_CMAQ-BENCHMARK"
    if [[ -r "${CROPMAP01}" ]] ; then
      export CROPMAP01
    else
      echo -e "${ERROR_PREFIX} cannot read CROPMAP01='${CROPMAP01}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
      exit 24
    fi

    CROPMAP04="${CROP_ACTIVITY_DIR}/EndPlanting_12km_CMAQ-BENCHMARK"
    if [[ -r "${CROPMAP04}" ]] ; then
      export CROPMAP04
    else
      echo -e "${ERROR_PREFIX} cannot read CROPMAP04='${CROPMAP04}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
      exit 25
    fi

    CROPMAP08="${CROP_ACTIVITY_DIR}/EndHarvesting_12km_CMAQ-BENCHMARK"
    if [[ -r "${CROPMAP08}" ]] ; then
      export CROPMAP08
    else
      echo -e "${ERROR_PREFIX} cannot read CROPMAP08='${CROPMAP08}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
      exit 26
    fi
  fi # if (( ${aglandon} > 0 ))

  ### start constants from CMAQ-5.0.1 benchmark (using same values)
  # should this not also be gated by (e.g.) aglandon? not in benchmark
  DUST_LU_1="${DUST_DIR}/beld3_CMAQ-BENCHMARK_output_a.ncf"
  if [[ -r "${DUST_LU_1}" ]] ; then
    export DUST_LU_1
  else
    echo -e "${ERROR_PREFIX} cannot read DUST_LU_1='${DUST_LU_1}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 27
  fi

  DUST_LU_2="${DUST_DIR}/beld3_CMAQ-BENCHMARK_output_tot.ncf"
  if [[ -r "${DUST_LU_2}" ]] ; then
    export DUST_LU_2
  else
    echo -e "${ERROR_PREFIX} cannot read DUST_LU_2='${DUST_LU_2}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 28
  fi
 
  ## ozone column data for the photolysis model
  OMI="${OMI_DIR}/OMI.dat"
  if [[ -r "${OMI}" ]] ; then
    export OMI
  else
    echo -e "${ERROR_PREFIX} cannot read OMI='${OMI}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 29
  fi

  ###   end constants from CMAQ-5.0.1 benchmark (using same values)

  # note bogus conditional in originals (08ab.*)
  GC_BCpath="${INPUT_ROOT_DIR}/roche/BC/2008cdc/AQplusN2O"
  if [[ -r "${GC_BCpath}" ]] ; then
    export GC_BCpath
  else
    echo -e "${ERROR_PREFIX} cannot read GC_BCpath='${GC_BCpath}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 30
  fi

  GC_ICpath_0="${INPUT_ROOT_DIR}/roche/IC/2008cdc/AQplusN2O" # path for FIRSTDAY ICs (afterward use yesterday's output)
  if [[ -r "${GC_ICpath_0}" ]] ; then
    export GC_ICpath_0
  else
    echo -e "${ERROR_PREFIX} cannot read GC_ICpath_0='${GC_ICpath_0}', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 31
  fi

#   export MPI='/share/linux86_64/mvapich2/mvapich2-1.4/install_x86_64_intel111059/bin' # infinity
#   # MPI='/usr/local/intel/impi/3.2.2.006/bin64'   # terrae
#   export MPIRUN="${MPI}/mpiexec"                 # infinity
#   # MPIRUN="${MPI}/mpirun"                        # terrae
  # get {MPI, MPIRUN} from *config.cmaq*: be consistent with build scripts

# ----------------------------------------------------------------------
# constants that DO change between OAQPS subruns
# ----------------------------------------------------------------------

  ### Run-variant constants set in following loop:
  ## Note PBS walltime differs per runned, so ideally we'd use one or more {dictionary, map, associative array}s, e.g.
  ## declare -A CCTM_RUNNED_TO_WALLTIME
  ## but the bash on infinity is version < 4 :-(
  ### WARNING: hafta maintain the following *_ARR in sync/parallel!
  ### TODO: rewrite this is in a higher-level language with multidimensional arrays!
  FIRSTDAY_ARR=(    '20071222' '20071222'  '20080621')
  YEAR_ARR=(        '2007'     '2008'      '2008')
  # don't ask me why the original code wanted this 3 ways. TODO: refactor!
  YEAR2_ARR=(        '2007'     '2008'      '2008')
  YR_ARR=(           '07'       '08'        '08')
  LASTDAY_ARR=(     '20071231' '20080630'  '20081231')
  INITIAL_RUN_ARR=( 'Y'        'N'         'Y')

  ### Output dir for each runned, on HPCC/infinity
  ### Set these each, manually, until we can get enough space on a single disk.
  CCTM_RUNNED_OUTDATA_ARR=(\
    "${OUTPUT_ROOT_DIR_ARR[0]}/roche/${OUTPUT_SUBPATH}/0" \
    "${OUTPUT_ROOT_DIR_ARR[1]}/roche/${OUTPUT_SUBPATH}/1" \
    "${OUTPUT_ROOT_DIR_ARR[2]}/roche/${OUTPUT_SUBPATH}/2" \
  )

  ### Populate runned's here to enable overloading in function test_wrapper_for_run
  CCTM_RUNNED_FP_ARR=() # path to each runned to call
  for (( SUBRUN_ITER=0; SUBRUN_ITER < ${#FIRSTDAY_ARR[@]}; SUBRUN_ITER++ )) ; do
    CCTM_RUNNED_FN="${CCTM_RUNNED_FN_PREFIX}_${FIRSTDAY_ARR[SUBRUN_ITER]}-${LASTDAY_ARR[SUBRUN_ITER]}.sh" # or run the original .csh?
    export CCTM_RUNNED_FP="${CCTM_RUNNED_DIR}/${CCTM_RUNNED_FN}"
    CCTM_RUNNED_FP_ARR[SUBRUN_ITER]="${CCTM_RUNNED_FP}"
  done # (( SUBRUN_ITER=0; SUBRUN_ITER < ${#FIRSTDAY_ARR[@]}; SUBRUN_ITER++ ))

  if [[ -n "${RUN_PBS}" ]] ; then
    PBS_WALLTIME_ARR=(  '68:00:00' '300:00:00' '300:00:00' ) # from OAQPS run on EMVL/terrae
    ### Populate in the subrun loop
    PBS_JOB_NAME_ARR=() # names for the runned/subruns
    PBS_LOG_FP_ARR=() # path to each runned's logfile
  fi

} # function setup_constants

# ----------------------------------------------------------------------
# set PBS/qsub constants
# ----------------------------------------------------------------------

function setup_qsub {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  PBS_QSUB_ARG='' # to start; will subsequently add args to head(PBS_QSUB_ARG)

  if [[ -z "${SUBRUN_ITER}" ]] ; then
    echo -e "${ERROR_PREFIX} SUBRUN_ITER not defined, exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 32
  else
    if (( ${SUBRUN_ITER} > 0 )) ; then
      # Gotta wait for previous subrun to finish. Do I have its ID?
      if [[ -n "${PBS_JOB_ID}" ]] ; then
        PBS_DEPEND="-W depend=afterok:${PBS_JOB_ID}"
      else
        echo -e "${ERROR_PREFIX} SUBRUN_ITER='${SUBRUN_ITER}' but !PBS_JOB_ID, exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
        exit 33
      fi
    fi
  fi # if [[ -z "${SUBRUN_ITER}" ]]

  ### set PBS/qsub resource list

  if   [[ -n "${N_NODES}" && -n "${N_PROCS_PER_NODE}" ]] ; then
    PBS_QSUB_CPU="nodes=${N_NODES}:ppn=${N_PROCS_PER_NODE}"
  elif [[ -n "${N_NODES}" ]] ; then
    PBS_QSUB_CPU="nodes=${N_NODES}"
  fi

  if   [[ -n "${PBS_WALLTIME_LIMIT}" ]] ; then
    PBS_QSUB_TIME="walltime=${PBS_WALLTIME_LIMIT}"
  fi

  if   [[ -n "${PBS_QSUB_CPU}" && -n "${PBS_QSUB_TIME}" ]] ; then
    PBS_QSUB_RESOURCES="${PBS_QSUB_CPU},${PBS_QSUB_TIME}"
  elif [[ -n "${PBS_QSUB_CPU}" && -z "${PBS_QSUB_TIME}" ]] ; then
    PBS_QSUB_RESOURCES="${PBS_QSUB_CPU}"
  elif [[ -z "${PBS_QSUB_CPU}" && -n "${PBS_QSUB_TIME}" ]] ; then
    PBS_QSUB_RESOURCES="${PBS_QSUB_TIME}"
  fi

  ### set PBS/qsub options
  ### does order matter in `qsub`s arg list?

  if [[ -n "${PBS_LOG_FP}" ]] ; then
    PBS_QSUB_ARG="-o ${PBS_LOG_FP} ${PBS_QSUB_ARG}"
  fi

  if [[ -n "${PBS_JOIN_OPTION}" ]] ; then
    PBS_QSUB_ARG="-j ${PBS_JOIN_OPTION} ${PBS_QSUB_ARG}"
  fi

  if [[ -n "${PBS_MAIL_OPTION}" ]] ; then
    PBS_QSUB_ARG="-m ${PBS_MAIL_OPTION} ${PBS_QSUB_ARG}"
  fi

  if [[ -n "${PBS_QSUB_RESOURCES}" ]] ; then
    PBS_QSUB_ARG="-l ${PBS_QSUB_RESOURCES} ${PBS_QSUB_ARG}"
  fi

  if [[ -n "${PBS_JOB_NAME}" ]] ; then
    PBS_QSUB_ARG="-N ${PBS_JOB_NAME} ${PBS_QSUB_ARG}"
  fi

  if [[ -n "${PBS_QUEUE_NAME}" ]] ; then
    PBS_QSUB_ARG="-q ${PBS_QUEUE_NAME} ${PBS_QSUB_ARG}"
  fi

  if [[ -n "${PBS_EXPORT_ENVIRONMENT}" ]] ; then
    PBS_QSUB_ARG="-V ${PBS_QSUB_ARG}"
  fi

  if [[ -n "${PBS_DEPEND}" ]] ; then
    PBS_QSUB_ARG="${PBS_DEPEND} ${PBS_QSUB_ARG}"
  fi

#  echo -e "${MESSAGE_PREFIX} PBS_QSUB_ARG='${PBS_QSUB_ARG}'" # debugging

} # function setup_qsub

# ----------------------------------------------------------------------
# wrap function=run for testing
# ----------------------------------------------------------------------

function test_wrapper_for_run {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local CCTM_RUNNED_FP='' # prevent inadvertent use later

  ### Set minimal PBS/qsub resources for testing
  if [[ -n "${RUN_PBS}" ]] ; then
    N_NODES='1'
    PBS_WALLTIME_ARR=( '1:00' '1:00' '1:00' )
  fi

  ### Create minimal runned scripts for testing "on the fly"
  for (( SUBRUN_ITER=0; SUBRUN_ITER < ${#FIRSTDAY_ARR[@]}; SUBRUN_ITER++ )) ; do
    CCTM_RUNNED_FN="test_wrapper_for_run_sub_${SUBRUN_ITER}.sh"
    CCTM_RUNNED_FP="${CCTM_RUNNED_DIR}/${CCTM_RUNNED_FN}"
    echo -e "echo -e 'Greetings from test subrun=${SUBRUN_ITER}!'" > "${CCTM_RUNNED_FP}"
    chmod u+x "${CCTM_RUNNED_FP}"
    CCTM_RUNNED_FP_ARR[SUBRUN_ITER]="${CCTM_RUNNED_FP}"
    CCTM_RUNNED_OUTDATA_ARR[SUBRUN_ITER]="${RUNNER_LOG_DIR}/${SUBRUN_ITER}"
  done

#  # start debugging
#   for (( SUBRUN_ITER=0; SUBRUN_ITER < ${#FIRSTDAY_ARR[@]}; SUBRUN_ITER++ )) ; do
#     CMD="cat ${CCTM_RUNNED_FP_ARR[SUBRUN_ITER]}"
#     echo -e "${MESSAGE_PREFIX} '${CMD}'==" 2>&1 | tee -a "${RUNNER_LOG_FP}"
#     eval "${CMD}" 2>&1 | tee -a "${RUNNER_LOG_FP}"
#   done
#   # start debugging

  # now call function=run
  run

} # function test_wrapper_for_run

# ----------------------------------------------------------------------
# show constants
# ----------------------------------------------------------------------

function show_constants {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -n "${RUN_PBS}" ]] ; then
    echo -e "${MESSAGE_PREFIX} PBS_LOG_FP='${PBS_LOG_FP}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
  fi
  echo -e "${MESSAGE_PREFIX} RUNNER_LOG_FP='${RUNNER_LOG_FP}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
  echo 2>&1 | tee -a "${RUNNER_LOG_FP}" # newline

  ### CCTM_OUTPUT_DIR in runner -> OUTDATA in runned
#  echo -e "${MESSAGE_PREFIX} CCTM_OUTPUT_DIR='${CCTM_OUTPUT_DIR}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
  echo -e "${MESSAGE_PREFIX} CCTM_RUNNED_FP='${CCTM_RUNNED_FP}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
  echo -e "${MESSAGE_PREFIX} NPCOL='${NPCOL}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
  echo -e "${MESSAGE_PREFIX} NPROW='${NPROW}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
  echo -e "${MESSAGE_PREFIX} NPCOL_NPROW='${NPCOL_NPROW}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
  echo -e "${MESSAGE_PREFIX} N_PROCS='${N_PROCS}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
  echo 2>&1 | tee -a "${RUNNER_LOG_FP}" # newline

  if [[ -n "${RUN_PBS}" ]] ; then
    echo -e "${MESSAGE_PREFIX} PBS_EXPORT_ENVIRONMENT='${PBS_EXPORT_ENVIRONMENT}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    echo -e "${MESSAGE_PREFIX} PBS_JOIN_OPTION='${PBS_JOIN_OPTION}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    echo -e "${MESSAGE_PREFIX} PBS_MAIL_OPTION='${PBS_MAIL_OPTION}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    echo -e "${MESSAGE_PREFIX} N_NODES='${N_NODES}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    echo -e "${MESSAGE_PREFIX} N_PROCS_PER_NODE='${N_PROCS_PER_NODE}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    echo -e "${MESSAGE_PREFIX} N_x_PPN='${N_x_PPN}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    echo -e "${MESSAGE_PREFIX} PBS_QUEUE_NAME='${PBS_QUEUE_NAME}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    echo -e "${MESSAGE_PREFIX} PBS_WALLTIME_LIMIT='${PBS_WALLTIME_LIMIT}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    echo -e "${MESSAGE_PREFIX} PBS_QSUB_ARG='${PBS_QSUB_ARG}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    echo 2>&1 | tee -a "${RUNNER_LOG_FP}" # newline
  fi # if [[ -n "${RUN_PBS}" ]]

} # function show_constants

function setup {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  # TODO: source CMAQ-build/{uber.config.cmaq.sh, config.cmaq.sh} to get {MPI, MPIRUN} (et al?)

  for CMD in \
    'test_top_constants' \
    'setup_constants' \
  ; do
    # no RUNNER_LOG_FP until we set it
    echo -e "${MESSAGE_PREFIX} '${CMD}'" # 2>&1 | tee -a "${RUNNER_LOG_FP}"
    # this fails: vars don't get exported (e.g., to function=show_constants)
    # eval "${CMD}" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    # this works: comment it out for NOPing, e.g., to `source`
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}': failed or not found\n" # 2>&1 | tee -a "${RUNNER_LOG_FP}"
      exit 34
    fi
    # no RUNNER_LOG_FP until we set it
    echo # 2>&1 | tee -a "${RUNNER_LOG_FP}" # newline
  done

} # function setup

# ----------------------------------------------------------------------
# run, either directly or under PBS
# ----------------------------------------------------------------------

function run {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ### Iterate over the subruns/runned's

  if [[ -z "${FIRSTDAY_ARR}" ]] ; then
    echo -e "${ERROR_PREFIX} FIRSTDAY_ARR is not defined (and you are using it to bound the subrun loop!), exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 35
  else

    for (( SUBRUN_ITER=0; SUBRUN_ITER < ${#FIRSTDAY_ARR[@]}; SUBRUN_ITER++ )) ; do
      export YEAR="${YEAR_ARR[SUBRUN_ITER]}"
      export YEAR2="${YEAR2_ARR[SUBRUN_ITER]}"
      export YR="${YR_ARR[SUBRUN_ITER]}"
      export OUTDIR="${CCTM_RUNNED_OUTDATA_ARR[SUBRUN_ITER]}"
      export FIRSTDAY="${FIRSTDAY_ARR[SUBRUN_ITER]}"
      export LASTDAY="${LASTDAY_ARR[SUBRUN_ITER]}"
      export INITIAL_RUN="${INITIAL_RUN_ARR[SUBRUN_ITER]}"
      CCTM_RUNNED_FP="${CCTM_RUNNED_FP_ARR[SUBRUN_ITER]}"

#       # start debugging
      echo -e "${MESSAGE_PREFIX} SUBRUN_ITER=${SUBRUN_ITER}:"  2>&1 | tee -a "${RUNNER_LOG_FP}"
#       echo -e "\trunned      =${CCTM_RUNNED_FP}"               2>&1 | tee -a "${RUNNER_LOG_FP}"
#       echo -e "\tFIRSTDAY    =${FIRSTDAY_ARR[SUBRUN_ITER]}"    2>&1 | tee -a "${RUNNER_LOG_FP}"
#       echo -e "\tLASTDAY     =${LASTDAY_ARR[SUBRUN_ITER]}"     2>&1 | tee -a "${RUNNER_LOG_FP}"
#       echo -e "\tINITIAL_RUN =${INITIAL_RUN_ARR[SUBRUN_ITER]}" 2>&1 | tee -a "${RUNNER_LOG_FP}"
#       echo -e "\tYEAR        =${YEAR}"                         2>&1 | tee -a "${RUNNER_LOG_FP}"
#       echo -e "\tOUTDIR      =${OUTDIR}"                       2>&1 | tee -a "${RUNNER_LOG_FP}"
#       #   end debugging

      if [[ -n "${RUN_PBS}" ]] ; then
        export PBS_WALLTIME_LIMIT="${PBS_WALLTIME_ARR[SUBRUN_ITER]}"
        export PBS_JOB_NAME="${PBS_JOB_NAME_PREFIX}_sub_${SUBRUN_ITER}"
        PBS_LOG_FN="${PBS_JOB_NAME}.log"
        export PBS_LOG_FP="${PBS_LOG_DIR}/${PBS_LOG_FN}"
        PBS_LOG_FP_ARR[SUBRUN_ITER]="${PBS_LOG_FP}"

#         # start debugging
#         echo -e "\twalltime    =${PBS_WALLTIME_LIMIT}"         2>&1 | tee -a "${RUNNER_LOG_FP}"
#         echo -e "\tPBS_JOB_NAME=${PBS_JOB_NAME}"               2>&1 | tee -a "${RUNNER_LOG_FP}"
#         echo -e "\tPBS_LOG_FP  =${PBS_LOG_FP}"                 2>&1 | tee -a "${RUNNER_LOG_FP}"
#         #   end debugging

      fi # if [[ -n "${RUN_PBS}" ]]

      ### Only know how to run PBS or ... not.

      if [[ -n "${RUN_PBS}" ]] ; then
        ## `setup_qsub` cannot run like [`show_constants`, `run_PBS`] because it ... sets up vars (<duh/>)
        ## and ... run_PBS has also gotta set a constant=PBS_JOB_ID.
        ## So just run the whole damn loop here.
#          'show_constants' \ # if desired before run_*
        for CMD in \
          'setup_qsub' \
          'run_PBS' \
        ; do
          echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" # 2>&1 | tee -a "${RUNNER_LOG_FP}"
          # this fails: vars don't get exported (e.g., to function=show_constants)
          # eval "${CMD}" 2>&1 | tee -a "${RUNNER_LOG_FP}"
          # this works: comment it out for NOPing, e.g., to `source`
          eval "${CMD}"
          if [[ $? -ne 0 ]] ; then
            echo -e "${ERROR_PREFIX} '${CMD}': failed or not found\n" # 2>&1 | tee -a "${RUNNER_LOG_FP}"
            exit 36
          fi
        done

      else # !RUN_PBS -> run_direct

        ## ASSERT: none of [`show_constants`, `run_direct`] set non-local vars
#          'show_constants' \ # if desired before run_*
        for CMD in \
          'run_direct' \
        ; do
          echo -e "${MESSAGE_PREFIX} '${CMD}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
          eval "${CMD}" 2>&1 | tee -a "${RUNNER_LOG_FP}"
          if [[ $? -ne 0 ]] ; then
            echo -e "${ERROR_PREFIX} '${CMD}': failed or not found\n" 2>&1 | tee -a "${RUNNER_LOG_FP}"
            exit 37
          fi
        done
      fi # if [[ -n "${RUN_PBS}" ]]

#       # start debugging
#       echo # newline
#       #   end debugging
    done # (( SUBRUN_ITER=0; SUBRUN_ITER < ${#FIRSTDAY_ARR[@]}; SUBRUN_ITER++ ))
  fi # if [[ -z "${FIRSTDAY_ARR}" ]]

} # function run

### NOT YET TESTED in this runner (just copied from run.cctm_runner.sh)
### and to be used only for testing this runner (which will not run direct)
function run_direct {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ -z "${OUTDIR}" ]] ; then
    echo -e "${ERROR_PREFIX} OUTDIR not defined, exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 38
  elif [[ ! -d "${OUTDIR}" ]] ; then
    for CMD in \
      "mkdir -p ${OUTDIR}/" \
    ; do
      echo -e "${MESSAGE_PREFIX} '${CMD}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
      eval "${CMD}" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    done
  elif [[ -z "${CCTM_RUNNED_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} CCTM_RUNNED_DIR not defined, exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 39
  elif [[ ! -d "${CCTM_RUNNED_DIR}" ]] ; then
    for CMD in \
      "mkdir -p ${CCTM_RUNNED_DIR}/" \
    ; do
      echo -e "${MESSAGE_PREFIX} '${CMD}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
      eval "${CMD}" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    done
  elif [[ -z "${CCTM_RUNNED_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} CCTM_RUNNED_FP not defined, exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 40
  elif [[ ! -x "${CCTM_RUNNED_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} CCTM_RUNNED_FP='${CCTM_RUNNED_FP}' not executable, exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 41
  fi # if   [[ -z "${OUTDIR}" ]]

  if   [[ ! -d "${OUTDIR}" ]] ; then
    echo -e "${ERROR_PREFIX} OUTDIR='${OUTDIR}' not readable, exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 42
  elif [[ ! -d "${CCTM_RUNNED_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} CCTM_RUNNED_DIR='${CCTM_RUNNED_DIR}' not readable, exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 43
  fi # if   [[ ! -d "${OUTDIR}" ]]

  for CMD in \
    "ls -alh ${RUNNER_LOG_FP}" \
    "ls -alt ${OUTDIR}/" \
    "du -hs ${OUTDIR}/" \
    "find ${OUTDIR}/ -type f | wc -l" \
    "ls -alh ${CCTM_RUNNED_FP}" \
    "pushd ${CCTM_RUNNED_DIR} ; ${CCTM_RUNNED_FP} ; popd" \
    "ls -alt ${OUTDIR}/" \
    "du -hs ${OUTDIR}/" \
    "find ${OUTDIR}/ -type f | wc -l" \
    "ls -alh ${RUNNER_LOG_FP}" \
  ; do
    echo -e "${MESSAGE_PREFIX} '${CMD}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}': failed or not found\n" 2>&1 | tee -a "${RUNNER_LOG_FP}"
      exit 44
    fi
    echo 2>&1 | tee -a "${RUNNER_LOG_FP}" # newline
  done

} # function run_direct

function run_PBS {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${OUTDIR}" ]] ; then
    echo -e "${ERROR_PREFIX} OUTDIR not defined, exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 45
  else
    echo -e "${MESSAGE_PREFIX} 'export'ing OUTDATA='${OUTDIR}" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    export OUTDATA="${OUTDIR}"
  fi

  if   [[ ! -d "${OUTDATA}" ]] ; then
    for CMD in \
      "mkdir -p ${OUTDATA}/" \
    ; do
      echo -e "${MESSAGE_PREFIX} '${CMD}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
      eval "${CMD}" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    done
  elif [[ -z "${CCTM_RUNNED_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} CCTM_RUNNED_FP not defined, exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 46
  elif [[ ! -x "${CCTM_RUNNED_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} CCTM_RUNNED_FP='${CCTM_RUNNED_FP}' not executable, exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 47
  elif [[ -z "${RUNNER_LOG_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} RUNNER_LOG_FP not defined, exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 48
  fi # if   [[ ! -d "${OUTDATA}" ]]

  if   [[ ! -d "${OUTDATA}" ]] ; then
    echo -e "${ERROR_PREFIX} OUTDATA='${OUTDATA}' not readable, exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 49
  fi

#    "qsub ${PBS_QSUB_ARG} ${CCTM_RUNNED_FP}" \ # gotta get PBS_JOB_ID from return
  for CMD in \
    "ls -alh ${RUNNER_LOG_FP}" \
    "find ${OUTDATA}/ -type f | wc -l" \
    "du -hs ${OUTDATA}/" \
    "ls -alt ${OUTDATA}/" \
  ; do
    echo -e "${MESSAGE_PREFIX} '${CMD}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${RUNNER_LOG_FP}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}': failed or not found\n" 2>&1 | tee -a "${RUNNER_LOG_FP}"
      exit 50
    fi
    echo 2>&1 | tee -a "${RUNNER_LOG_FP}" # newline
  done

  ### Run the job, get its ID from PBS: see http://stackoverflow.com/a/17773866/915044
  export N_SUBRUN="${SUBRUN_ITER}"
  CMD="qsub ${PBS_QSUB_ARG} ${CCTM_RUNNED_FP}"
  echo -e "${MESSAGE_PREFIX} '${CMD}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
  PBS_QSUB_RETVAL="$(eval "${CMD}")"

  if [[ -z "${PBS_QSUB_RETVAL}" ]] ; then
    echo -e "${ERROR_PREFIX} failed to get return value from 'qsub', exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 51
  else
    PBS_JOB_ID="${PBS_QSUB_RETVAL%%.*}" # delete everything including and after the first '.'
  fi

  if [[ -z "${PBS_JOB_ID}" ]] ; then
#    PBS_JOB_ID="${SUBRUN_ITER}" # FOR DEBUGGING ONLY
    echo -e "${ERROR_PREFIX} failed to parse 'qsub' return value for PBS_JOB_ID, exiting ..." 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 52
  else
    echo -e "${MESSAGE_PREFIX} SUBRUN_ITER='${SUBRUN_ITER}' -> PBS_JOB_ID='${PBS_JOB_ID}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
  fi

  cat <<EOM 2>&1 | tee -a "${RUNNER_LOG_FP}"
To inspect PBS job:
  overview all running jobs:
    date ; qstat
  more detail on running jobs:
    date ; qstat -f | grep -e 'Id:\|Name\|Owner\|Resource_List'
  most detail on running jobs:
    date ; qstat -f
  show this job:
    date ; qstat ${PBS_JOB_ID}
  show fuller info about this job:
    date ; qstat -f ${PBS_JOB_ID}

To inspect progress of PBS job:
  inspect both logs (this, PBS, et possibly al):
    date ; ls -alt ${THIS_DIR}/*.log
  inspect output dir/folder:
    date ; ls -alt ${OUTDIR}/
    date ; find ${OUTDIR}/ -type f | wc -l
    date ; du -hs ${OUTDIR}/
  inspect PBS log (won't flush til job ends, ab- or normally):
    date ; wc -l < ${PBS_LOG_FP}
    date ; tail ${PBS_LOG_FP}

To kill PBS job:
  qdel ${PBS_JOB_ID} # won't kill instantaneously

To cleanup after killing PBS job:
  inspect output dir/folder:
    date ; ls -alt ${OUTDIR}/
  delete output:
    rm ${OUTDIR}/*
  inspect logs:
    date ; ls -alt ${THIS_DIR}/*.log
  delete logs (only!):
    rm ${THIS_DIR}/*.log

Preferred scriptlet for job monitoring:
JOB_ID='${PBS_JOB_ID}'
PBS_LOG_FP='${PBS_LOG_FP}'
OUTDIR='${OUTDIR}'
QSTAT_CMD="qstat -f \${JOB_ID}"
date ; \${QSTAT_CMD} | head ; \${QSTAT_CMD} | tail ; ls -alt ${OUTDIR}/ ; ls -al ${PBS_LOG_FP}

To show all my jobs:
  qstat -u $(whoami)

EOM

} # function run_PBS

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

# relocated above, after setting RUNNER_LOG_FP
# START="$(date)"
# # no RUNNER_LOG_FP until we set it
# echo -e "${MESSAGE_PREFIX} START=${START}" # 2>&1 | tee -a "${RUNNER_LOG_FP}"
# echo # 2>&1 | tee -a "${RUNNER_LOG_FP}" # newline

MESSAGE_PREFIX="${THIS_FN}::main loop:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#  'setup' \   # sources, resources
#  'test_wrapper_for_run' \
#  'run' \     # for real
#  'inspect' \ # TODO: sleep first?
#  'teardown'  # as needed
for CMD in \
  'setup' \
  'run' \
; do
  echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" # 2>&1 | tee -a "${RUNNER_LOG_FP}"
  # this fails: vars don't get exported (e.g., to function=show_constants)
  # eval "${CMD}" 2>&1 | tee -a "${RUNNER_LOG_FP}"
  # this works: comment it out for NOPing, e.g., to `source`
  eval "${CMD}"
  if [[ $? -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} '${CMD}': failed or not found\n" # 2>&1 | tee -a "${RUNNER_LOG_FP}"
    exit 53
  fi
done

# By now, RUNNER_LOG_FP should be set
echo 2>&1 | tee -a "${RUNNER_LOG_FP}" # newline
echo -e "${MESSAGE_PREFIX}   END=$(date)" 2>&1 | tee -a "${RUNNER_LOG_FP}"
echo -e "${MESSAGE_PREFIX} START=${START}" 2>&1 | tee -a "${RUNNER_LOG_FP}"
echo 2>&1 | tee -a "${RUNNER_LOG_FP}" # newline

exit 0 # victory! ... at least, for the non-PBS part of the job :-)
