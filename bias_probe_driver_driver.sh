#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Get information about bias/differences between my/generated and reference/tarballed output data.
### Currently drives bias_probe_driver.sh, which drives the real work.

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### Get these out of the way before we get to work:
THIS="$0"
# THIS_DIR="$(dirname ${THIS})"
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

## logging-related
DATETIME_FORMAT='%Y%m%d_%H%M'
DATETIME_STAMP="$(date +${DATETIME_FORMAT})"
LOG_FN="${THIS_FN}_${DATETIME_STAMP}.log"
# logfile defined below, local to raw data
# LOG_FP="${THIS_DIR}/${LOG_FN}"

CMAQ_ROOT='/project/inf35w/roche/CMAQ-5.0.1'
RAW_DATA_ROOT="${CMAQ_ROOT}/tarsplat-transplant/CMAQv5.0.1/data/cctm/benchmark/new"
REF_DATA_DIR="${CMAQ_ROOT}/tarsplat/CMAQv5.0.1/data/ref/cctm"
BIAS_DATA_ROOT="$(echo -e ${RAW_DATA_ROOT} | sed -e 's/new$/bias/')"
WORKER_FP="${CMAQ_ROOT}/git/old/CCTM-build/bias_probe_driver.sh"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

OUTER_START="$(date)"
echo -e "start ${THIS_FN}=${OUTER_START}" # screen only 2>&1 | tee -a "${LOG_FP}"
echo # screen only 2>&1 | tee -a "${LOG_FP}" # newline

for DATA_LEAF in \
  'parallel/4x4' \
  'serial/direct' \
  'serial/PBS' \
; do
  RAW_DATA_DIR="${RAW_DATA_ROOT}/${DATA_LEAF}"
  BIAS_DATA_DIR="${BIAS_DATA_ROOT}/${DATA_LEAF}"
  LOG_FP="${RAW_DATA_DIR}/${LOG_FN}"
# start debugging
#  echo -e "${MESSAGE_PREFIX} RAW_DATA_DIR='${RAW_DATA_DIR}'"
#  echo -e "${MESSAGE_PREFIX} BIAS_DATA_DIR='${BIAS_DATA_DIR}'"
#  echo -e "${MESSAGE_PREFIX} LOG_FP='${LOG_FP}'"
#   end debugging

  if [[ -d "${BIAS_DATA_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} bias data dir/folder='${BIAS_DATA_DIR}' exists: rename or remove. Exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  else
    for CMD in \
      "mkdir -p ${BIAS_DATA_DIR}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi

  if [[ -d "${RAW_DATA_DIR}" ]] ; then
    export RAW_DATA_DIR
    export BIAS_DATA_DIR
    export LOG_FP

    INNER_START="$(date)"
    echo -e "start ${DATA_LEAF}=${INNER_START}" 2>&1 | tee -a "${LOG_FP}"
    echo 2>&1 | tee -a "${LOG_FP}" # newline

    for CMD in \
      "${WORKER_FP}" \
    ; do
      echo -e "${MESSAGE_PREFIX} '${CMD}' -> '${DATA_LEAF}'" 2>&1 | tee -a "${LOG_FP}"
#        eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
#        if [[ ${PIPESTATUS[0]} -ne 0 ]] ; then # test the *pipeline*, not the `tee`
# Above was writing duplicate output to log, both of command line and command output :-(
# Accordingly must pass LOG_FP to users (i.e., bias_probe_driver.sh).
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${ERROR_PREFIX} '${CMD}' failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
        exit 3
      else
	echo 2>&1 | tee -a "${LOG_FP}" # newline
	echo -e "  end ${DATA_LEAF}=$(date)" 2>&1 | tee -a "${LOG_FP}"
	echo -e "start ${DATA_LEAF}=${INNER_START}" 2>&1 | tee -a "${LOG_FP}"
	echo 2>&1 | tee -a "${LOG_FP}" # newline
      fi
    done
  else
    echo -e "${ERROR_PREFIX} cannot find RAW_DATA_DIR='${RAW_DATA_DIR}'" # so can't write log
    exit 4
  fi
done # for DATA_LEAF

# screen only
echo # newline
echo -e "  end ${THIS_FN}=$(date)"
echo -e "start ${THIS_FN}=${OUTER_START}"
echo # newline

exit 0 # success!
