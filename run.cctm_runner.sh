#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Run run.cctm under PBS (serial or parallel) or not (serial only).
### User must (currently) edit constants below (TODO: properties file)
### * in block='constants'
### * in `function setup_constants`

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### message-related
THIS="$0"
# THIS_DIR="$(dirname ${THIS})"
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"

### major question: are we running under PBS or not? ASSERT: must be set by caller
### TODO: extend for other job managers (SGE, LSF, Condor, etc)
### TODO: find a more positive way of doing bash truth, so [[ -z "${RUN_PBS}" ]] can throw error.

### major question: where to write output? ASSERT: must be set by caller
if [[ -z "${CCTM_OUTPUT_DIR}" ]] ; then # allow override by wrappers, commandline
  echo -e "${ERROR_PREFIX} output root CCTM_OUTPUT_DIR undefined, exiting ..."
  exit 2
fi

### major question: how is the IOAPI processor grid defined?
if [[ -z "${NPCOL}" ]] ; then # allow override by wrappers, commandline
  echo -e "${ERROR_PREFIX} IOAPI column processor count NPCOL undefined, exiting ..."
  exit 2
fi
if [[ -z "${NPROW}" ]] ; then # allow override by wrappers, commandline
  echo -e "${ERROR_PREFIX} IOAPI row processor count NPROW undefined, exiting ..."
  exit 2
fi

### logging-related
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
DATETIME_FORMAT='%Y%m%d_%H%M'
DATETIME_STAMP="$(date +${DATETIME_FORMAT})"
if [[ -z "${DRIVER_LOG_FP}" ]] ; then
  DRIVER_LOG_FN="${THIS_FN}_${DATETIME_STAMP}.log"
  DRIVER_LOG_DIR="${CCTM_OUTPUT_DIR}" # must be defined here!
  export DRIVER_LOG_FP="${DRIVER_LOG_DIR}/${DRIVER_LOG_FN}"
fi # else take predefined DRIVER_LOG_FP

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

if   [[ -z "${RUN_PBS}" ]] ; then
  echo -e "${MESSAGE_PREFIX} running directly (RUN_PBS undefined)"
else # debugging, really
  echo -e "${MESSAGE_PREFIX} will run under PBS (RUN_PBS='${RUN_PBS}')"
fi

### NOTE: currently cannot write both this and PBS to same log! see
### http://www.supercluster.org/pipermail/torqueusers/2013-November/016421.html
if   [[ -z "${DRIVER_LOG_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} log path DRIVER_LOG_FP undefined, exiting ..."
  exit 2
elif [[ -r "${DRIVER_LOG_FP}" ]] ; then
  echo -e "${ERROR_PREFIX} log='${DRIVER_LOG_FP}' exists. Is another job running? Exiting JIC ..."
  exit 3
else # debugging, really. DON'T tee yet! could be writing log to output dir
  echo -e "${MESSAGE_PREFIX} will write to log='${DRIVER_LOG_FP}'"
  if [[ -n "${RUN_PBS}" ]] ; then
    echo -e "${MESSAGE_PREFIX} note PBS will write to separate log!"
  fi
fi

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

### user: edit here!
function setup_constants {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ### Are we running PBS or not/unmanaged/serial?
  # if (( ${RUN_PBS} )) ; then
  # TODO: bash: how to check definition and value in one test?
  if [[ -n "${RUN_PBS}" ]] ; then
    if [[ -z "${PBS_JOB_NAME}" ]] ; then # allow override by wrappers, commandline
      PBS_JOB_NAME="${THIS_FN}__${DATETIME_STAMP}"
    fi
    echo -e "${MESSAGE_PREFIX} running under PBS, PBS_JOB_NAME='${PBS_JOB_NAME}'" # not yet logged

    ### gotta separate logs for PBS and driver(s), since PBS will overwrite
    # TODO: PBS logging: find out how to make PBS append
    if [[ -n "${RUN_PBS}" ]] ; then
      PBS_LOG_FN="${PBS_JOB_NAME}.log"
      # keep logs with data
      PBS_LOG_DIR="${CCTM_OUTPUT_DIR}"
      export PBS_LOG_FP="${PBS_LOG_DIR}/${PBS_LOG_FN}"
    fi

  else # [[ -z "${RUN_PBS}" ]]
    echo -e "${MESSAGE_PREFIX} running direct/native/unmanaged/serial" # not yet logged
  fi
  echo # newline

  ### non-PBS helpers and spaces
#  CCTM_RUNNER_DIR="${CMAQ_ROOT}/CMAQv5.0.1/scripts/cctm"
  CCTM_RUNNER_DIR="${THIS_DIR}"
  CCTM_RUNNER_FN='run.cctm'
  CCTM_RUNNER_FP="${CCTM_RUNNER_DIR}/${CCTM_RUNNER_FN}"
  # ORIG_CCTM_RUNNER_FP="${CCTM_RUNNER_FP}.0" # debugging: for comparison, below
  export OUTDIR="${CCTM_OUTPUT_DIR}"  # set for run.cctm
  ## TODO: exit if OUTDIR has files?

  ### cores/processors and nodes
  if [[ -n "${RUN_PBS}" ]] ; then
    ## for IOAPI: horizontal domain decomposition
    # CMAQ-5.0.1 benchmark: values from tarsplat run.cctm: 
    # NPCOL='4' # columns of processor grid
    # NPROW='3' #    rows of processor grid
    # NPROCS == total number of cores/processors across all nodes
    export NPROCS=$(( ${NPCOL} * ${NPROW} ))
    export NPCOL_NPROW="${NPCOL} ${NPROW}"

    ## for PBS: host decomposition
    # CMAQ-5.0.1 benchmark: given met domination by westerlies, hope PBS allocates 1 row/node!
    if [[ -z "${N_NODES}" ]] ; then # allow override by wrappers, commandline
      N_NODES="${NPROW}"
    fi
    if [[ -z "${N_PROCS_PER_NODE}" ]] ; then # allow override by wrappers, commandline
      N_PROCS_PER_NODE="${NPCOL}"
    fi
    (( N_x_PPN=${N_NODES} * ${N_PROCS_PER_NODE} )) # bash arithmetic expansion syntax
    if (( ${N_x_PPN} != ${NPROCS} )); then
      echo -e "${ERROR_PREFIX} N_x_PPN='${N_x_PPN}' != NPROCS='${NPROCS}', exiting ..."
      exit 5
    fi

  else # [[ -z "${RUN_PBS}" ]]
    # not running PBS -> only run serial, cannot override:
    NPCOL='1'
    NPROW='1'
    export NPROCS='1'
    export NPCOL_NPROW='1 1'
  fi # if [[ -n "${RUN_PBS}" ]]

  ### other PBS-related vars (except PBS_JOB_NAME above)
  if [[ -n "${RUN_PBS}" ]] ; then
    PBS_WALLTIME_LIMIT='10:00:00'  # HH:MM:SS, took 04:49:55 on login node
    PBS_QUEUE_NAME='batch'         # only one available on HPCC/infinity?
    PBS_EXPORT_ENVIRONMENT='-V'    # blank to not export caller's envvars
    PBS_JOIN_OPTION='oe'           # log join: put both stdout and stderr into the same output/log file]
    PBS_MAIL_OPTION='n'            # can't send email outside the firewall from HPCC/infinity? or any EPA host?
    export PBS_WORKDIR="${CCTM_RUNNER_DIR}" # not a normal PBS envvar: I added this to make the damn thing go where I want (see logfile or commit message)
  fi # if [[ -n "${RUN_PBS}" ]]

} # function setup_constants

# ----------------------------------------------------------------------
# test top-level constants
# ----------------------------------------------------------------------

function test_top_constants {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ -z "${CCTM_OUTPUT_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} CCTM_OUTPUT_DIR undefined, exiting ..." 2>&1 | tee -a "${DRIVER_LOG_FP}"
    exit 6
  elif [[ -d "${CCTM_OUTPUT_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} output dir='${CCTM_OUTPUT_DIR}' exists: rename or delete before running job. Exiting ..."
    exit 7
  else # CCTM_OUTPUT_DIR does not exist: make it

    for CMD in \
      "mkdir -p ${CCTM_OUTPUT_DIR}" \
    ; do
      # may not be able to log before output dir exists
      echo -e "${MESSAGE_PREFIX} '${CMD}'" # 2>&1 | tee -a "${DRIVER_LOG_FP}"
      eval "${CMD}" # 2>&1 | tee -a "${DRIVER_LOG_FP}"
    done
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} '${CMD}': failed or not found\n" 2>&1 | tee -a "${DRIVER_LOG_FP}"
      exit 8
    fi

    if [[ ! -d "${CCTM_OUTPUT_DIR}" ]] ; then
      echo -e "${ERROR_PREFIX} cannot make output dir/folder='${CCTM_OUTPUT_DIR}', exiting ..." # 2>&1 | tee -a "${DRIVER_LOG_FP}"
      exit 9
    fi
  fi # [[ -z "${CCTM_OUTPUT_DIR}" ]]

  ### ASSERT: by now we should be able to write log

  if   [[ -z "${CCTM_RUNNER_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} CCTM_RUNNER_DIR undefined, exiting ..." 2>&1 | tee -a "${DRIVER_LOG_FP}"
    exit 10
  elif [[ ! -d "${CCTM_RUNNER_DIR}" ]] ; then
      echo -e "${ERROR_PREFIX} cannot find runner dir/folder='${CCTM_RUNNER_DIR}', exiting ..." 2>&1 | tee -a "${DRIVER_LOG_FP}"
      exit 11
  elif [[ -z "${CCTM_RUNNER_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} CCTM_RUNNER_FP undefined, exiting ..." 2>&1 | tee -a "${DRIVER_LOG_FP}"
    exit 12
  elif [[ ! -x "${CCTM_RUNNER_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot execute CCTM runner='${CCTM_RUNNER_FP}', exiting ..." 2>&1 | tee -a "${DRIVER_LOG_FP}"
    exit 13
  elif [[ -z "${NPROCS}" ]] ; then
    echo -e "${ERROR_PREFIX} NPROCS undefined, exiting ..." 2>&1 | tee -a "${DRIVER_LOG_FP}"
    exit 14

#   elif [[ ! -r "${ORIG_CCTM_RUNNER_FP}" ]] ; then
#     echo -e "${ERROR_PREFIX} cannot read original CCTM runner='${ORIG_CCTM_RUNNER_FP}', exiting ..." 2>&1 | tee -a "${DRIVER_LOG_FP}"
#     exit 15
  fi

  if [[ -n "${RUN_PBS}" ]] ; then
    if   [[ -z "${PBS_LOG_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} PBS_LOG_FP undefined, exiting ..." 2>&1 | tee -a "${DRIVER_LOG_FP}"
      exit 16
    elif [[ -r "${PBS_LOG_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} PBS log='${PBS_LOG_FP}' exists. Is another job running? Exiting ..." 2>&1 | tee -a "${DRIVER_LOG_FP}"
      exit 17
    fi
  else # [[ -z "${RUN_PBS}" ]] i.e. direct/native/unmanaged/serial
    if (( ${NPROCS} != 1 )) ; then
      echo -e "${ERROR_PREFIX} not running PBS, but NPROCS='${NPROCS}' != 1, exiting ..."
      exit 18
    fi
  fi

} # function test_top_constants

# ----------------------------------------------------------------------
# set PBS/qsub constants
# ----------------------------------------------------------------------

function setup_qsub {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -n "${RUN_PBS}" ]] ; then

    ### does order matter in `qsub`s arg list?
    PBS_QSUB_ARG='' # to start; will subsequently add args to head(PBS_QSUB_ARG)

    ### set PBS/qsub resource list

    if   [[ -n "${N_NODES}" && -n "${N_PROCS_PER_NODE}" ]] ; then
      PBS_QSUB_CPU="nodes=${N_NODES}:ppn=${N_PROCS_PER_NODE}"
    elif [[ -n "${N_NODES}" ]] ; then
      PBS_QSUB_CPU="nodes=${N_NODES}"
    fi

    if   [[ -n "${PBS_WALLTIME_LIMIT}" ]] ; then
      PBS_QSUB_TIME="walltime=${PBS_WALLTIME_LIMIT}"
    fi

    if   [[ -n "${PBS_QSUB_CPU}" && -n "${PBS_QSUB_TIME}" ]] ; then
      PBS_QSUB_RESOURCES="${PBS_QSUB_CPU},${PBS_QSUB_TIME}"
    elif [[ -n "${PBS_QSUB_CPU}" && -z "${PBS_QSUB_TIME}" ]] ; then
      PBS_QSUB_RESOURCES="${PBS_QSUB_CPU}"
    elif [[ -z "${PBS_QSUB_CPU}" && -n "${PBS_QSUB_TIME}" ]] ; then
      PBS_QSUB_RESOURCES="${PBS_QSUB_TIME}"
    fi

    ### set PBS/qsub options

    if [[ -n "${PBS_LOG_FP}" ]] ; then
      PBS_QSUB_ARG="-o ${PBS_LOG_FP} ${PBS_QSUB_ARG}"
    fi

    if [[ -n "${PBS_JOIN_OPTION}" ]] ; then
      PBS_QSUB_ARG="-j ${PBS_JOIN_OPTION} ${PBS_QSUB_ARG}"
    fi

    if [[ -n "${PBS_MAIL_OPTION}" ]] ; then
      PBS_QSUB_ARG="-m ${PBS_MAIL_OPTION} ${PBS_QSUB_ARG}"
    fi

    if [[ -n "${PBS_QSUB_RESOURCES}" ]] ; then
      PBS_QSUB_ARG="-l ${PBS_QSUB_RESOURCES} ${PBS_QSUB_ARG}"
    fi

    if [[ -n "${PBS_JOB_NAME}" ]] ; then
      PBS_QSUB_ARG="-N ${PBS_JOB_NAME} ${PBS_QSUB_ARG}"
    fi

    if [[ -n "${PBS_QUEUE_NAME}" ]] ; then
      PBS_QSUB_ARG="-q ${PBS_QUEUE_NAME} ${PBS_QSUB_ARG}"
    fi

    if [[ -n "${PBS_EXPORT_ENVIRONMENT}" ]] ; then
      PBS_QSUB_ARG="-V ${PBS_QSUB_ARG}"
    fi

  fi # if [[ -n "${RUN_PBS}" ]]

} # function setup_qsub

# ----------------------------------------------------------------------
# show constants
# ----------------------------------------------------------------------

function show_constants {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  echo -e "${MESSAGE_PREFIX} CCTM_OUTPUT_DIR='${CCTM_OUTPUT_DIR}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
  echo -e "${MESSAGE_PREFIX} CCTM_RUNNER_FP='${CCTM_RUNNER_FP}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
  echo -e "${MESSAGE_PREFIX} NPCOL='${NPCOL}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
  echo -e "${MESSAGE_PREFIX} NPCOL_NPROW='${NPCOL_NPROW}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
  echo -e "${MESSAGE_PREFIX} NPROCS='${NPROCS}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
  echo -e "${MESSAGE_PREFIX} NPROW='${NPROW}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
  # echo -e "${MESSAGE_PREFIX} ORIG_CCTM_RUNNER_FP='${ORIG_CCTM_RUNNER_FP}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
  echo -e "${MESSAGE_PREFIX} DRIVER_LOG_FP='${DRIVER_LOG_FP}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
  echo 2>&1 | tee -a "${DRIVER_LOG_FP}" # newline

  if [[ -n "${RUN_PBS}" ]] ; then
    echo -e "${MESSAGE_PREFIX} PBS_EXPORT_ENVIRONMENT='${PBS_EXPORT_ENVIRONMENT}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    echo -e "${MESSAGE_PREFIX} PBS_JOIN_OPTION='${PBS_JOIN_OPTION}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    echo -e "${MESSAGE_PREFIX} PBS_MAIL_OPTION='${PBS_MAIL_OPTION}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    echo -e "${MESSAGE_PREFIX} N_NODES='${N_NODES}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    echo -e "${MESSAGE_PREFIX} N_PROCS_PER_NODE='${N_PROCS_PER_NODE}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    echo -e "${MESSAGE_PREFIX} N_x_PPN='${N_x_PPN}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    echo -e "${MESSAGE_PREFIX} PBS_QSUB_RESOURCES='${PBS_QSUB_RESOURCES}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    echo -e "${MESSAGE_PREFIX} PBS_QUEUE_NAME='${PBS_QUEUE_NAME}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    echo -e "${MESSAGE_PREFIX} PBS_WALLTIME_LIMIT='${PBS_WALLTIME_LIMIT}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    echo 2>&1 | tee -a "${DRIVER_LOG_FP}" # newline
    echo -e "${MESSAGE_PREFIX} PBS_QSUB_ARG='${PBS_QSUB_ARG}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    echo 2>&1 | tee -a "${DRIVER_LOG_FP}" # newline
  fi # if [[ -n "${RUN_PBS}" ]]
} # function show_constants

function setup {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  for CMD in \
    'setup_constants' \
    'test_top_constants' \
  ; do
#    echo -e "\n$ ${MESSAGE_PREFIX} '${CMD}'\n" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    echo -e "$ ${MESSAGE_PREFIX} '${CMD}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    # this fails: vars don't get exported (e.g., to function=inspect_all_output)
#    eval "${CMD}" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    # this works: comment it out for NOPing, e.g., to `source`
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX}: '${CMD}': failed or not found\n" 2>&1 | tee -a "${DRIVER_LOG_FP}"
      exit 19
    fi
    echo 2>&1 | tee -a "${DRIVER_LOG_FP}" # newline
  done

  if [[ -n "${RUN_PBS}" ]] ; then
    for CMD in \
      'setup_qsub' \
    ; do
      echo -e "$ ${MESSAGE_PREFIX} '${CMD}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${ERROR_PREFIX}: '${CMD}': failed or not found\n" 2>&1 | tee -a "${DRIVER_LOG_FP}"
        exit 20
      fi
      echo 2>&1 | tee -a "${DRIVER_LOG_FP}" # newline
    done
  fi

  # for debugging, or just to CYA
  for CMD in \
    'show_constants' \
  ; do
    echo -e "$ ${MESSAGE_PREFIX} '${CMD}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${DRIVER_LOG_FP}" # ok IFF these set no constants
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX}: '${CMD}': failed or not found\n" 2>&1 | tee -a "${DRIVER_LOG_FP}"
      exit 21
    fi
    echo 2>&1 | tee -a "${DRIVER_LOG_FP}" # newline
  done

} # function setup

# ----------------------------------------------------------------------
# run, either directly or under PBS
# ----------------------------------------------------------------------

function run {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -n "${RUN_PBS}" ]] ; then
    CMD_SUFFIX='PBS'
  else
    CMD_SUFFIX='direct'
  fi

  for CMD in "run_${CMD_SUFFIX}" ; do
#    echo -e "\n$ ${MESSAGE_PREFIX} '${CMD}'\n" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    echo -e "$ ${MESSAGE_PREFIX} '${CMD}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX}: '${CMD}': failed or not found\n" 2>&1 | tee -a "${DRIVER_LOG_FP}"
      exit 22
    fi
    echo 2>&1 | tee -a "${DRIVER_LOG_FP}" # newline
  done

} # function run

function run_direct {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#    "mkdir -p ${OUTDIR}/"                               # just in case
#    "pushd ${CCTM_RUNNER_DIR}" \
#    "${CCTM_RUNNER_FP}" \
#    'popd' \
  for CMD in \
    "ls -alh ${DRIVER_LOG_FP}" \
    "mkdir -p ${OUTDIR}/" \
    "ls -alt ${OUTDIR}/" \
    "du -hs ${OUTDIR}/" \
    "find ${OUTDIR}/ -type f | wc -l" \
    "pushd ${CCTM_RUNNER_DIR} ; ${CCTM_RUNNER_FP} ; popd" \
    "ls -alt ${OUTDIR}/" \
    "du -hs ${OUTDIR}/" \
    "find ${OUTDIR}/ -type f | wc -l" \
    "ls -alh ${DRIVER_LOG_FP}" \
  ; do
    echo -e "${MESSAGE_PREFIX} '${CMD}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX}: '${CMD}': failed or not found\n" 2>&1 | tee -a "${DRIVER_LOG_FP}"
      exit 23
    fi
    echo 2>&1 | tee -a "${DRIVER_LOG_FP}" # newline
  done

} # function run_direct

function run_PBS {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#    "pushd ${CCTM_RUNNER_DIR}"    # doesn't work for PBS: see PBS_WORKDIR above
#    "mkdir -p ${OUTDIR}/"                               # just in case
  for CMD in \
    "ls -alh ${DRIVER_LOG_FP}" \
    "mkdir -p ${OUTDIR}/" \
    "find ${OUTDIR}/ -type f | wc -l" \
    "du -hs ${OUTDIR}/" \
    "ls -alt ${OUTDIR}/" \
    "qsub ${PBS_QSUB_ARG} ${CCTM_RUNNER_FP}" \
  ; do
    echo -e "${MESSAGE_PREFIX} '${CMD}'" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX}: '${CMD}': failed or not found\n" 2>&1 | tee -a "${DRIVER_LOG_FP}"
      exit 24
    fi
    echo 2>&1 | tee -a "${DRIVER_LOG_FP}" # newline
  done

  cat <<EOM 2>&1 | tee -a "${DRIVER_LOG_FP}"
To inspect PBS job:
  show all running jobs:
    date ; qstat
  show this job:
    date ; qstat [job ID echoed by 'qsub' above]
  show fuller info about this job:
    date ; qstat -f [job ID echoed by 'qsub' above]

To inspect progress of PBS job:
  inspect both logs (this, PBS, et possibly al):
    date ; ls -alt ${THIS_DIR}/*.log
  inspect output dir/folder:
    date ; ls -alt ${OUTDIR}/
    date ; find ${OUTDIR}/ -type f | wc -l
    date ; du -hs ${OUTDIR}/
  inspect PBS log (won't flush til job ends, ab- or normally):
    date ; wc -l < ${PBS_LOG_FP}
    date ; tail ${PBS_LOG_FP}

To kill PBS job:
  qdel [job ID echoed by 'qsub' above] # won't kill instantaneously

To cleanup after killing PBS job:
  inspect output dir/folder:
    date ; ls -alt ${OUTDIR}/
  delete output:
    rm ${OUTDIR}/*
  inspect logs:
    date ; ls -alt ${THIS_DIR}/*.log
  delete logs (only!):
    rm ${THIS_DIR}/*.log

Preferred scriptlet for job monitoring:
JOB_ID='' # fillin
PBS_LOG_FP='${PBS_LOG_FP}'
OUTDIR='${OUTDIR}'
QSTAT_CMD="qstat -f \${JOB_ID}"
date ; \${QSTAT_CMD} | head ; \${QSTAT_CMD} | tail ; ls -alt ${OUTDIR}/ ; ls -al ${PBS_LOG_FP}

EOM

} # function run_PBS

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

START="$(date)"
echo -e "START ${THIS_FN}=${START}" 2>&1 | tee -a "${DRIVER_LOG_FP}"
echo 2>&1 | tee -a "${DRIVER_LOG_FP}" # newline

MESSAGE_PREFIX="${THIS_FN}::main loop:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#  'setup' \   # sources, resources
#  'run' \
#  'inspect' \ # TODO: sleep first?
#  'teardown'  # as needed
for CMD in \
  'setup' \
  'run' \
; do
  echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${DRIVER_LOG_FP}"
# this fails: vars don't get exported (e.g., to function=inspect_all_output)
#  eval "${CMD}" 2>&1 | tee -a "${DRIVER_LOG_FP}"
# this works: comment it out for NOPing, e.g., to `source`
  eval "${CMD}"
  if [[ $? -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} '${CMD}': failed or not found\n" 2>&1 | tee -a "${DRIVER_LOG_FP}"
    exit 25
  fi
done

echo 2>&1 | tee -a "${DRIVER_LOG_FP}" # newline
echo -e "  END ${THIS_FN}=$(date)" 2>&1 | tee -a "${DRIVER_LOG_FP}"
echo -e "START ${THIS_FN}=${START}" 2>&1 | tee -a "${DRIVER_LOG_FP}"
echo 2>&1 | tee -a "${DRIVER_LOG_FP}" # newline

exit 0 # victory! ... at least, for the non-PBS part of the job :-)
