#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Build project=CCTM: i.e., produce a CCTM executable.
### For details see https://bitbucket.org/epa_n2o_project_team/cctm-build/src/HEAD/README.md

### Expects to be run on a linux (tested with RHEL5). Requires
### * not-too-up-to-date `bash` (tested with version=3.2.25!)
### * reasonably up-to-date `git` (tested with version= 1.7.4.1)
### * `basename`
### * `date`
### * `dirname`
### * `find`
### * `head`
### * `ls`
### * `pwd`
### * `readlink`
### * `tail`
### * `tee`

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### Get these out of the way before we get to work:
## message-related
THIS="$0"
# THIS_DIR="$(dirname ${THIS})"
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${THIS_FN}: ERROR:"

## logging-related
DATETIME_FORMAT='%Y%m%d_%H%M'
DATETIME_STAMP="$(date +${DATETIME_FORMAT})"
LOG_FN="${THIS_FN}_${DATETIME_STAMP}.log"
# log is local to this script, not build space
LOG_FP="${THIS_DIR}/${LOG_FN}"
# SUPER_NAME='CCTM-build' # name of the superproject

### more constants in function=setup* below 

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# common code for build and run
source "${THIS_DIR}/CCTM_utilities.sh" # for function=setup_CCTM_common_vars

function setup_CCTM_build {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

# let bldit.cctm do these:
#    'document_CCTM_build_vars' \
#    'setup_CCTM_sources' \
#    'setup_ICL_sources' \
#    'setup_MECHS_sources' \
  for CMD in \
    "setup_CCTM_common_vars ${LOG_FP}" \
    'setup_CCTM_build_vars' \
    'setup_CCTM_build_space' \
    'check_CCTM_build_space' \
  ; do
    echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "${MESSAGE_PREFIX} ${CMD}: ERROR: failed or not found\n"
      exit 2
    fi
  done
} # function setup_CCTM_build

function setup_CCTM_build_vars {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ### build prerequisites:

  # repo=BLDMAKE
  export BLDMAKE_DIR="${M3HOME}/BLDMAKE"
  # TODO: move knowledge of `bldmake` executable up to CMAQ-build/*config.cmaq*
  BLDMAKE_EXEC_FN='bldmake'
  export BLDMAKE_EXEC_FP="${BLDMAKE_DIR}/${BLDMAKE_EXEC_FN}"
  if   [[ -z "${BLDMAKE_EXEC_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} BLDMAKE_EXEC_FP not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 3
  elif [[ ! -x "${BLDMAKE_EXEC_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} bldmake='${BLDMAKE_EXEC_FP}' not executable, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 4
  fi

  ## repo=CCTM. TODO: test!
  CCTM_DIR="${M3MODEL}" # also ${M3HOME}/CCTM, also THIS_DIR. TODO? check equality?
  export REPOROOT="${CCTM_DIR}" # Makefile wants this name

  # repo=CCTM-build. TODO: test! TODO: gotta disambiguate this from the name of the directory where we build executable=CCTM
  CCTM_BUILD_DIR="${M3HOME}/CCTM-build"
  CCTM_BUILD_EXEC="${CCTM_BUILD_DIR}/bldit.cctm"

  ## repo=ICL. TODO: test!
  ICL_DIR="${M3HOME}/ICL"

  ## repo=MECHS. TODO: test!
  MECHS_DIR="${M3HOME}/MECHS"
  # export MECH_INC="${MECHS_DIR}/${Mechanism}" # Makefile wants this name

} # function setup_CCTM_build_vars

### create build space
function setup_CCTM_build_space {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ -z "${CCTM_BUILD_DIR_PATH}" ]] ; then
    echo -e "${ERROR_PREFIX} CCTM_BUILD_DIR_PATH not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  elif [[ -d "${CCTM_BUILD_DIR_PATH}" ]] ; then
    echo -e "${ERROR_PREFIX} build dir='${CCTM_BUILD_DIR_PATH}' exists (move or delete it before running this script), exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 6
  else
    for CMD in \
      "mkdir -p ${CCTM_BUILD_DIR_PATH}" \
    ; do
      echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "${ERROR_PREFIX} ${CMD}: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
        exit 7
      fi
    done
  fi # [[ -z "${CCTM_BUILD_DIR_PATH}" ]]

  ## Copy our config.cmaq to CCTM for use by bldit.cctm
  if   [[ ! -r "${CONFIG_CMAQ_PATH}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot find '${CONFIG_CMAQ_PATH}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 8
  elif [[ ! -d "${CCTM_BUILD_DIR_PATH}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot find CCTM build dir='${CCTM_BUILD_DIR_PATH}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 9
  else
    for CMD in \
      "cp ${CONFIG_CMAQ_PATH} ${CCTM_BUILD_DIR_PATH}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
    ## ... then tell CCTM where to find config.cmaq
    export CONFIG_CMAQ_PATH="${CCTM_BUILD_DIR_PATH}/config.cmaq"
  fi # [[ -r "${CONFIG_CMAQ_PATH}" ]]

} # function setup_CCTM_build_space

function check_CCTM_build_space {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ### Don't build if ...

  ## ... we already have the executable we seek to build.
  ## Unfortunately the name and even the dir of the target executable is determined in ${CCTM_BUILD_EXEC}, which is csh, and gets called later.
  ## TODO: move setting of path of target executable up!
  if [[ -x "${CCTM_EXEC_FP}" ]] ; then
    ls -al "${CCTM_EXEC_FP}"
    echo -e "${ERROR_PREFIX} CCTM executable '${CCTM_EXEC_FP}' exists: move or delete." 2>&1 | tee -a "${LOG_FP}"
    exit 10
  fi

  ## ... we have nothing from which to build.

  # CCTM sources
  if [[ ! -d "${M3MODEL}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot find repo with CCTM sources @ '${M3MODEL}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 11
  fi

  # CCTM includes
  if [[ ! -d "${ICL_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot find repo with CCTM includes @ '${ICL_DIR}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 12
  fi

  # CCTM mechanisms
  if [[ ! -d "${MECHS_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot find repo with CCTM mechanisms @ '${MECHS_DIR}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 13
  fi

  ## ... we have nothing with which to build.

  if [[ ! -d "${CCTM_BUILD_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot find repo=CCTM-build @ '${CCTM_BUILD_DIR}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 14
  fi

  if [[ ! -x "${CCTM_BUILD_EXEC}" ]] ; then
    echo -e "${ERROR_PREFIX} CCTM_BUILD_EXEC='${CCTM_BUILD_EXEC}' not executable. Exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 15
  fi

  # TODO: rewrite CMAQ build scripts in bash!
  if [[ ! -x "${CSH_EXEC}" ]] ; then
    echo -e "${ERROR_PREFIX} CSH_EXEC='${CSH_EXEC}' not executable. Exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 16
  fi

  if   [[ -z "${CONFIG_CMAQ_PATH}" ]] ; then
    echo -e "${ERROR_PREFIX} CONFIG_CMAQ_PATH not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 17
  elif [[ "$(dirname ${CONFIG_CMAQ_PATH})" != "${CCTM_BUILD_DIR_PATH}" ]] ; then
    echo -e "${ERROR_PREFIX} '${CONFIG_CMAQ_PATH}' not in subdir of '${CCTM_BUILD_DIR_PATH}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 18
  fi # [[ -z "${CONFIG_CMAQ_PATH}" ]]

  ## (TODO: consume tag:
  ##  * Checkout desired tag of CCTM-build
  ##  * Recreate branch=master (after checkout-ing tag), otherwise we're detached HEAD (due to tag)
  ##  1. Delete local branch=master: "${GIT_REMOTE_PREFIX} git checkout tags/${SUPER_TAG}"
  ##  2. Delete remote branch=master ?: "${GIT_REMOTE_PREFIX} git branch -D master"
  ##  3. Create local branch=master: "${GIT_REMOTE_PREFIX} git checkout -b master"
  ## )

} # function check_CCTM_build_space

### Use ${CCTM_BUILD_EXEC} to build source repo=CCTM with include repos={ICL, MECHS}
function build_CCTM {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ## why does `pushd` fail inside `eval` loops, but ONLY WHEN PIPING OUTPUT ???
  echo -e "$ pushd ${CCTM_BUILD_DIR}" 2>&1 | tee -a "${LOG_FP}"
  pushd "${CCTM_BUILD_DIR}"

  # build with loop lines, then
  #  "ls -alt $(ls -1dt $(find ${CCTM_EXEC_DIR} -maxdepth 1 -type d | grep -ve "${CCTM_EXEC_DIR}$") | head -n 1) | head" \
  # to find CCTM_EXEC
  for CMD in \
    "ls -alt ${CCTM_BUILD_DIR_PATH}" \
    "${CCTM_BUILD_EXEC}" \
    "popd" \
    "ls -al ${CCTM_EXEC_FP}" \
    "ls -al ${LOG_FP}" \
    "ls -alt ${CCTM_BUILD_DIR_PATH}" \
  ; do
    echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
    if [[ -n "$(tail -n 3 ${LOG_FP} | grep -ie 'error\|fatal\|fail' | grep -ve 'fail-fast\|failfast')" ]] ; then
      echo -e "${ERROR_PREFIX} failed to '${CMD}'. Exiting..." 2>&1 | tee -a "${LOG_FP}"
      exit 19
    fi
  done # for CMD

} # function build_CCTM

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

MESSAGE_PREFIX="${THIS_FN}::main loop:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#  'setup_CCTM_build' \
#  'build_CCTM' \
for CMD in \
  'setup_CCTM_build' \
  'build_CCTM' \
; do
#   "ls -alhR ${M3HOME}" \
  echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n"
  eval "${CMD}" # comment this out for NOPing, e.g., to `source`
  if [[ $? -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} failed or not found\n"
    exit 20
  fi
done

exit 0 # success!
