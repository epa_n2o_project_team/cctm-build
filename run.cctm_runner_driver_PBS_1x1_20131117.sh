#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Overload a few key items before calling run.cctm_runner.sh to make benchmark run
### TODO: check output (sleep if PBS-ing)

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

THIS="$0"
# THIS_DIR="$(dirname ${THIS})"
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"

### serial under PBS
export NPCOL='1'
export NPROW='1'
export N_NODES="${NPROW}"
export N_PROCS_PER_NODE="${NPCOL}"
export RUN_PBS='yes!'

if [[ -z "${RUN_PBS}" ]] ; then
  OUTPUT_LEAF='serial/direct'
else
  NPROCS=$(( ${NPCOL} * ${NPROW} ))
  if [[ "${NPROCS}" == '1' ]] ; then # TODO: use arithmetic comparison
    OUTPUT_LEAF='serial/PBS'
  else
    OUTPUT_LEAF="parallel/${NPROW}x${NPCOL}"
  fi
fi
OUTPUT_TAG="$(echo -e ${OUTPUT_LEAF} | sed -e 's|/|_|g')"
export PBS_JOB_NAME="tarsplat_run.cctm_${OUTPUT_TAG}"

### on HPCC/infinity
export CCTM_OUTPUT_DIR="/project/inf35w/roche/CMAQ-5.0.1/tarsplat-transplant/CMAQv5.0.1/data/cctm/benchmark/new/${OUTPUT_LEAF}"
### ASSERT: if (exists(it)) then whack(it) # it's failed
if [[ -d "${CCTM_OUTPUT_DIR}" ]] ; then
  rm -fr ${CCTM_OUTPUT_DIR}
fi

### the call
${THIS_DIR}/run.cctm_runner.sh
### the response
exit 0 # victory! ... at least, for the non-PBS part of the job :-)
