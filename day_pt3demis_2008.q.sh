#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### /work/romo/2008cdc/CMAQv4.7.1/2008ab_08c_N5ao_inline/12US1/job/day_pt3demis.q was a csh script,
### written by US EPA OAQPS for their CDC/PHASE-2008 run of CMAQ:
### for details on that run, see
### https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/EPA_CDC_PHASE_run_for_2008
### Mechanically translated to bash by Tom Roche.
### It should still do inline plume rise emissions processing.

THIS="$0"
THIS_DIR="$(readlink -f $(dirname ${THIS}))" # absolute path
# THIS_FN="$(basename ${THIS})" # no! under PBS, this becomes, e.g., '27600.imaster.amad.gov.SC'
THIS_FN='day_pt3demis_2008.q.sh'
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

# if [[ $STDAT == '08' ]] ; then
#   gline=9
# elif [[ $STDAT == '09' ]] ; then
#   gline=10
# else 
#   gline=$(( ${STDAT} + 1 ))
# fi
# why mess with a formatted string? use global=day (set by runned loop)
gline=$(( ${day} + 1 ))

# set refdir  = /work/romo/terra_test/CMAQv4.7.1/smoke_out/em_v4/inputs/ge_dat/smk_dates/2008 # original, for terrae
refdir='/project/inf35w/roche/emis/2008cdc/AQonly/PSE/refdir'
# Should this be setting the global var=APPL?
# if [[ "${MONTH}" == '8' || "${MONTH}" == '9' ]] ; then
#   APPL="${YEAR}0${MONTH}${STDAT}"
#   reffile="${refdir}/smk_merge_dates_${YEAR}0${MONTH}.txt"
# else
#   APPL="${YEAR}${MONTH}${STDAT}"
#   reffile="${refdir}/smk_merge_dates_${YEAR}${MONTH}.txt"
# fi
reffile="${refdir}/smk_merge_dates_${YEAR}${MM}.txt"

if [[ -r "${reffile}" ]] ; then
  LINES_reffile="$(wc -l < "${reffile}")"
  if (( ${gline} <= ${LINES_reffile} )) ; then
    echo -e "${MESSAGE_PREFIX} about to seek line=${gline} of reffile='${reffile}'"
  else
    echo -e "${ERROR_PREFIX} seeking line=${gline} of reffile='${reffile}', which only has lines=${LINES_reffile}. Exiting ..."
    exit 1
  fi
else
  echo -e "${ERROR_PREFIX} cannot read reffile='${reffile}', exiting ..."
  exit 2
fi

intable="$(head -${gline} ${reffile} | tail -1)"
if [[ -n "${intable}" ]] ; then
  ## string is delimited by ', ' (both comma and space) so remove commas before splitting
  INTABLE_ARR=( $(echo -e ${intable} | sed -e 's/,//g') )
#      Date="$(echo -e ${intable[0]} | cut -d, -f1)"
      Date="${INTABLE_ARR[0]}"
  aveday_N="${INTABLE_ARR[1]}"
  aveday_Y="${INTABLE_ARR[2]}"
   mwdss_N="${INTABLE_ARR[3]}"
   mwdss_Y="${INTABLE_ARR[4]}"
    week_N="${INTABLE_ARR[5]}"
    week_Y="${INTABLE_ARR[6]}"
       all="${INTABLE_ARR[7]}"
  # start debugging
  echo -e "${MESSAGE_PREFIX} intable='${intable}'"
  echo -e "${MESSAGE_PREFIX} aveday_N='${aveday_N}'"
  echo -e "${MESSAGE_PREFIX} aveday_Y='${aveday_Y}'"
  echo -e "${MESSAGE_PREFIX}  mwdss_N='${mwdss_N}'"
  echo -e "${MESSAGE_PREFIX}  mwdss_Y='${mwdss_Y}'"
  # end debugging
else
  echo -e "${ERROR_PREFIX} could not access line=${gline} of reffile='${reffile}', exiting ..."
  exit 1
fi

# set IN_PTpath2 = /work/romo/2008cdc/smoke_out/2008aa_08c/12US1/cmaq_cb05_soa # original, for terrae
IN_PTpath2='/project/inf35w/roche/emis/2008cdc/AQonly/PSE'
CASE2='12US1_cmaq_cb05_soa_2008aa_08c'

# SECTOR defined in set_pt3demis_2008.q.sh
iefile1="inln_mole_${SECTOR[0]}_${mwdss_Y}_${CASE2}.ncf"
iefile2="inln_mole_${SECTOR[1]}_${APPL}_${CASE2}.ncf"
iefile3="inln_mole_${SECTOR[2]}_${mwdss_N}_${CASE2}.ncf"
iefile4="inln_mole_${SECTOR[3]}_${aveday_N}_${CASE2}.ncf"
iefile5="inln_mole_${SECTOR[4]}_${APPL}_${CASE2}.ncf"

STK_EMIS_01="${IN_PTpath2}/${SECTOR[0]}/${iefile1}"
STK_EMIS_02="${IN_PTpath2}/${SECTOR[1]}/${iefile2}"
STK_EMIS_03="${IN_PTpath2}/${SECTOR[2]}/${iefile3}"
STK_EMIS_04="${IN_PTpath2}/${SECTOR[3]}/${iefile4}"
STK_EMIS_05="${IN_PTpath2}/${SECTOR[4]}/${iefile5}"
## setenv STK_EMIS_06 $IN_PTpath2/${SECTOR[6]}/$iefile6 # commented in original

if [[ -r "${STK_EMIS_01}" ]] ; then
  export STK_EMIS_01
  echo -e "${MESSAGE_PREFIX} STK_EMIS_01='${STK_EMIS_01}'"
else
  echo -e "${ERROR_PREFIX} cannot read STK_EMIS_01='${STK_EMIS_01}', exiting ..."
  exit 2
fi

if   [[ -r "${STK_EMIS_02}" ]] ; then
  echo -e "${MESSAGE_PREFIX} STK_EMIS_02='${STK_EMIS_02}'"
  export STK_EMIS_02
  ### We have a known problem with missing files for 2007 for ptfire and ptipm.
  ### So error unless those conditions hold (until we fix that @#$%^&! problem)
elif [[ "${STK_EMIS_02#*2007}" != "${STK_EMIS_02}" && \
        ( "${STK_EMIS_02#*ptfire}" != "${STK_EMIS_02}" || \
          "${STK_EMIS_02#*ptipm}" != "${STK_EMIS_02}" ) \
     ]] ; then
  echo -e "${MESSAGE_PREFIX} cannot read STK_EMIS_02='${STK_EMIS_02}', error?"
else
  echo -e "${ERROR_PREFIX} cannot read STK_EMIS_02='${STK_EMIS_02}', exiting ..."
  exit 5
fi

if [[ -r "${STK_EMIS_03}" ]] ; then
  export STK_EMIS_03
  echo -e "${MESSAGE_PREFIX} STK_EMIS_03='${STK_EMIS_03}'"
else
  echo -e "${ERROR_PREFIX} cannot read STK_EMIS_03='${STK_EMIS_03}', exiting ..."
  exit 4
fi

if [[ -r "${STK_EMIS_04}" ]] ; then
  export STK_EMIS_04
  echo -e "${MESSAGE_PREFIX} STK_EMIS_04='${STK_EMIS_04}'"
else
  echo -e "${ERROR_PREFIX} cannot read STK_EMIS_04='${STK_EMIS_04}', exiting ..."
  exit 5
fi

if   [[ -r "${STK_EMIS_05}" ]] ; then
  echo -e "${MESSAGE_PREFIX} STK_EMIS_05='${STK_EMIS_05}'"
  export STK_EMIS_05
  ### We have a known problem with missing files for 2007 for ptfire and ptipm.
  ### So error unless those conditions hold (until we fix that @#$%^&! problem)
elif [[ "${STK_EMIS_05#*2007}" != "${STK_EMIS_05}" && \
        ( "${STK_EMIS_05#*ptfire}" != "${STK_EMIS_05}" || \
          "${STK_EMIS_05#*ptipm}" != "${STK_EMIS_05}" ) \
     ]] ; then
  echo -e "${MESSAGE_PREFIX} cannot read STK_EMIS_05='${STK_EMIS_05}', error?"
else
  echo -e "${ERROR_PREFIX} cannot read STK_EMIS_05='${STK_EMIS_05}', exiting ..."
  exit 5
fi

if [[ -n "${PT3DFRAC}" ]] ; then   # $PT3DFRAC is defined
  if [[ ${PT3DFRAC} == 'Y' || ${PT3DFRAC} == 'T' ]] ; then
    pt3dfrac=1
  else
    pt3dfrac=0
  fi
else   # $PT3DFRAC is not defined => $PT3DFRAC == 'N'
  pt3dfrac=0
fi

if (( ${pt3dfrac} > 0 )) ; then
  #> output layer fractions (play) files
  DATE="${EMISDATE}"
  (( grp = 1 ))
  while (( ${grp} <= ${NPTGRPS} )) ; do
    file="${OUTDIR}/cctm_play_${DATE}.${SECTOR[${grp}]}.${APPL}"
    export PLAY_BASE0${grp}_="${file}"
    if [[ "${DISP}" == 'delete' ]] ; then
      /bin/rm -f ${file}_???.ncf
    fi
    (( grp++ ))
  done
  #> play srcid files to reconstruct a global play file
  (( count = 0 ))
  while (( ${count} <= ${NPROCS} )) ; do
    if (( ${count} <= 9 )) ; then
      sub="0${count}"
    else
      sub="${count}"
    fi
    (( grp = 1 ))
    while (( ${grp} <= ${NPTGRPS} )) ; do
      file="${OUTDIR}/lay_src_id_0${grp}.${DATE}.${APPL}_${sub}"
      export SRCIDLAY0${grp}_${sub}="${file}"
      if [[ "${DISP}" == 'delete' ]] ; then
        /bin/rm -f ${file}
      fi
      (( grp++ ))
    done
    (( count++ ))
  done
fi

#> report of plumes exceeding layer REP_LAYER_MAX ...
export REP_LAYER_MIN=0   # Minimum layer for reporting plume rise info [0=off]
#setenv REP_LAYER_MIN 7
if (( ${REP_LAYER_MIN} != 0 )) ; then
  set -x # set echo
  DATE="${EMISDATE}"
  (( count = 0 ))
  while (( ${count} <= ${NPROCS} )) ; do
    if (( ${count} <= 9 )) ; then
      sub="0${count}"
    else
      sub="${count}"
    fi
    (( grp = 1 ))
    while (( ${grp} <= ${NPTGRPS} )) ; do
      file="${OUTDIR}/rptlay_0${grp}.${DATE}.${APPL}_${sub}"
      export REPRTLAY_0${grp}${sub}="${file}"
      if [[ "${DISP}" == 'delete' ]] ; then
        /bin/rm -f ${file}
      fi
      (( grp++ ))
    done
    (( count++ ))
  done
  set +x # unset echo
fi
