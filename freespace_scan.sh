#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Utility to scan freespace on servers we use to run CMAQ: NOT part of the build/run.
### Requires PBS/Torque utility=`pbsnodes` to discover cluster hosts.

### TODO:
### * support sort on space available

#---------------------------------------------------------------------- 
# code
#---------------------------------------------------------------------- 

echo -e "Filesystem\tSize\tAvail\tMount" # output header

# Why not do something like
# > for NODE_RECORD in $(pbsnodes -a | tr '\n' '%' | sed -e 's/%%/\n/g' | sort) ; do
# ? Because the above creates/returns 1 string containing multiple lines (including their newlines),
# but we want a *set* of individual\separate lines.
while read NODE_RECORD ; do
 NODE_HOST="${NODE_RECORD%%%*}" # remove everything after the first '%'
 NODE_NAME="${NODE_HOST%%.*}"   # remove everything after the first '.'
#  echo -e "${NODE_NAME}"         # debugging
 NODE_N="${NODE_NAME##inode}"   # remove the initial 'inode'
#  echo -e "${NODE_N}"            # debugging
 NODE_DIR="/project/inf${NODE_N}w"
 if [[ -r "${NODE_DIR}" ]] ; then
   for CMD in \
     "df -h ${NODE_DIR}/ 2> /dev/null | awk -F' ' '{ print \$1, \"\t\", \$2, \"\t\", \$4, \"\\t\", \$6 }' | tail -n 1" \
   ; do
#      echo -e "$ ${CMD}"
     eval "${CMD}"
   done
 else
   echo -e "${NODE_DIR} cannot be read"
 fi
# convert 5-line "stanzas" into single-line records internally delimited by '%'
# done <(pbsnodes -a | tr '\n' '%' | sed -e 's/%%/\n/g') # this fails
done < <(pbsnodes -a | tr '\n' '%' | sed -e 's/%%/\n/g')
