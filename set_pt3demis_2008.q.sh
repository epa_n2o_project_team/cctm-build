#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### /work/romo/2008cdc/CMAQv4.7.1/2008ab_08c_N5ao_inline/12US1/job/set_pt3demis.q was a csh script,
### written by US EPA OAQPS for their CDC/PHASE-2008 run of CMAQ:
### for details on that run, see
### https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/EPA_CDC_PHASE_run_for_2008
### Mechanically translated to bash by Tom Roche.
### It should still do inline plume rise emissions processing.

THIS="$0"
THIS_DIR="$(readlink -f $(dirname ${THIS}))" # absolute path
# THIS_FN="$(basename ${THIS})" # no! under PBS, this becomes, e.g., '27600.imaster.amad.gov.SC'
THIS_FN='set_pt3demis_2008.q.sh'
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#> number of input emission sector file groups [ 1 ] (max of 9 in this script)
SECTOR=(\
  'ptnonipm' \
  'ptipm' \
  'othpt' \
  'c3marine' \
  'ptfire' )
export NPTGRPS=${#SECTOR[@]}

#> inline plume rise point source emissions file sector representative date matrix
#>  (jun 2005)   M  T  W Th  F Sa Su
# set Sect01Day = (    07 07 07 11 12 \
#                06 07 07 07 07 11 12 \
#                06 07 07 07 07 11 11 \
#                06 07 07 07 07 11 12 \
#                06 07 07 07 )
# set Sect03Day = (    08 09 10 11 12 \
#                06 07 08 09 10 11 12 \
#                06 07 08 09 10 11 12 \
#                06 07 08 09 10 11 12 \
#                06 07 07 07 )
# set Sect05Day = 07
#> the other 2 sectors emissions files have dates corresponding to the actual date
#> (set in the while loop)

# set IN_STACKpath = /work/romo/2008cdc/smoke_out/2008aa_08c/12US1/cmaq_cb05_soa # original, on terrae
IN_STACKpath='/project/inf35w/roche/emis/2008cdc/AQonly/PSE'
STKCASE1='12US1_2008aa_08c'
# set STKCASE2 = 12EUS1_2005ck_05b
# set STKCASE3 = 12EUS1_2020ck1_14_35b_05b

#> month constant files # comment in original
isfile1="stack_groups_${SECTOR[0]}_${STKCASE1}.ncf"
isfile2="stack_groups_${SECTOR[1]}_${STKCASE1}.ncf"
isfile3="stack_groups_${SECTOR[2]}_${STKCASE1}.ncf"
isfile4="stack_groups_${SECTOR[3]}_${STKCASE1}.ncf"
isfile5="stack_groups_${SECTOR[4]}_${EMISDATE}_${STKCASE1}.ncf"

STK_GRPS_01="${IN_STACKpath}/${isfile1}"
STK_GRPS_02="${IN_STACKpath}/${isfile2}"
STK_GRPS_03="${IN_STACKpath}/${isfile3}"
STK_GRPS_04="${IN_STACKpath}/${isfile4}"
STK_GRPS_05="${IN_STACKpath}/${isfile5}"

if [[ -r "${STK_GRPS_01}" ]] ; then
  echo -e "${MESSAGE_PREFIX} STK_GRPS_01='${STK_GRPS_01}'"
  export STK_GRPS_01
else
  echo -e "${ERROR_PREFIX} cannot read STK_GRPS_01='${STK_GRPS_01}', exiting ..."
  exit 1
fi

if [[ -r "${STK_GRPS_02}" ]] ; then
  echo -e "${MESSAGE_PREFIX} STK_GRPS_02='${STK_GRPS_02}'"
  export STK_GRPS_02
else
  echo -e "${ERROR_PREFIX} cannot read STK_GRPS_02='${STK_GRPS_02}', exiting ..."
  exit 2
fi

if [[ -r "${STK_GRPS_03}" ]] ; then
  echo -e "${MESSAGE_PREFIX} STK_GRPS_03='${STK_GRPS_03}'"
  export STK_GRPS_03
else
  echo -e "${ERROR_PREFIX} cannot read STK_GRPS_03='${STK_GRPS_03}', exiting ..."
  exit 3
fi

if [[ -r "${STK_GRPS_04}" ]] ; then
  echo -e "${MESSAGE_PREFIX} STK_GRPS_04='${STK_GRPS_04}'"
  export STK_GRPS_04
else
  echo -e "${ERROR_PREFIX} cannot read STK_GRPS_04='${STK_GRPS_04}', exiting ..."
  exit 4
fi

if   [[ -r "${STK_GRPS_05}" ]] ; then
  echo -e "${MESSAGE_PREFIX} STK_GRPS_05='${STK_GRPS_05}'"
  export STK_GRPS_05
  ### We have a known problem with missing files for 2007 for ptfire and ptipm.
  ### So error unless those conditions hold (until we fix that @#$%^&! problem)
elif [[ "${STK_GRPS_05#*2007}" != "${STK_GRPS_05}" && \
        ( "${STK_GRPS_05#*ptfire}" != "${STK_GRPS_05}" || \
          "${STK_GRPS_05#*ptipm}" != "${STK_GRPS_05}" ) \
     ]] ; then
  echo -e "${MESSAGE_PREFIX} cannot read STK_GRPS_05='${STK_GRPS_05}', error?"
else
  echo -e "${ERROR_PREFIX} cannot read STK_GRPS_05='${STK_GRPS_05}', exiting ..."
  exit 5
fi

# following all commented in original
# setenv STK_GRPS_06 $IN_STACKpath/${SECTOR[6]}/$isfile6
#set iefile5 = inln_mole_${SECTOR[5]}_${YEAR}${MONTH}${Sect05Day}_${CASE}.ncf
#setenv STK_EMIS_05 $IN_PTpath/$iefile5
#setenv IPVERT       1  # Method for vertical spread [0]
