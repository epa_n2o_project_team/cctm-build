### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Code common to building and running project=CCTM.
### For details see https://bitbucket.org/epa_n2o_project_team/cctm-build/src/HEAD/README.md

### Expects to be run on a linux (tested with RHEL5). Requires
### * not-too-up-to-date `bash` (tested with version=3.2.25!)
### * `dirname`
### * `find`

# ----------------------------------------------------------------------

### Setup envvars common to both build and run.
### Takes 1 arg: path to logfile

### This code is quite similar to BCON-build/BCON_utilities.sh. TODO: refactor.
### Note this code "scrapes" echo from the csh-based config.cmaq, which is painful.
### TODO: just use bash-based config.cmaq.sh for all repo build/run.
### This also sets vars still set in bldit.cctm. TODO: move those out of bldit.cctm.
function setup_CCTM_common_vars {
  local LOG_FP="$1"
  local THIS_FN='CCTM_utilities.sh'
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ### Find and load, (and scrape if necessary) envvars from *config.cmaq*

  ### I expect all project repos to be peer dirs: see CMAQ-build/repo_creator.sh
  CMAQ_BUILD_DIR="$(dirname ${THIS_DIR})/CMAQ-build"
  # For discussion of config.cmaq, see
  # http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD#Configuring_your_system_for_compiling_CMAQ
  # we depend on the csh version, since we do a bldmake-based build
  export CONFIG_CMAQ_PATH="${CMAQ_BUILD_DIR}/config.cmaq"
  export CSH_EXEC='/bin/tcsh' # since config.cmaq is (currently) a csh script.
  # SUPER_TAG='5.0.1_release_built_infinity_20130918' # desired `git` tag for superproject

  ### Most important environment variables (envvars) for config.cmaq:
  ### see CMAQ-build/uber.config.cmaq.sh for details
  export UBER_CONFIG_CMAQ_PATH="${CMAQ_BUILD_DIR}/uber.config.cmaq.sh"

  ### M3DATA not needed for build, and neither COMPILER nor INFINITY needed for run.
  ### However, one really should be running after build, if only to test.
  # TODO: test COMPILER as enum.
  # TODO: nuke the INFINITY kludge, or test as binary.
  if [[ -z "${COMPILER}" || -z "${INFINITY}" || -z "${M3DATA}" || -z "${M3LIB}" || -z "${M3HOME}" || -z "${M3MODEL}" ]] ; then
    if [[ -r "${UBER_CONFIG_CMAQ_PATH}" ]] ; then
      source "${UBER_CONFIG_CMAQ_PATH}" # cannot log/tee?
    else
      echo -e "${ERROR_PREFIX} cannot find uber.config.cmaq, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 2
    fi
  fi

  ### but override M3MODEL for this project
  export M3MODEL="${M3HOME}/CCTM" # repo with the CCTM sources

  if [[ -z "${COMPILER}" || -z "${INFINITY}" || -z "${M3DATA}" || -z "${M3LIB}" || -z "${M3HOME}" || -z "${M3MODEL}" ]] ; then
    # still?
    echo -e "${ERROR_PREFIX} one of COMPILER, INFINITY, M3DATA, M3LIB, M3HOME, M3MODEL is not defined, exiting ..."
    exit 2
  fi # [[ -z "${M3LIB}" ||

  if   [[ ! -d "${M3HOME}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read M3HOME='${M3HOME}', exiting ..."
    exit 2
  elif [[ ! -d "${M3LIB}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read M3LIB='${M3LIB}', exiting ..."
    exit 2
  elif [[ ! -d "${M3DATA}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read M3DATA='${M3DATA}', exiting ..."
    exit 2
  fi

  if [[ ! -r "${CONFIG_CMAQ_PATH}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot find '${CONFIG_CMAQ_PATH}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  else
#     source "${CONFIG_CMAQ_PATH}" # cannot log/tee?
    # nope, config.cmaq is csh
    echo -e "${MESSAGE_PREFIX} START: 'source ${CONFIG_CMAQ_PATH}' using '${CSH_EXEC}'" 2>&1 | tee -a "${LOG_FP}"
#       echo -e "source ${CONFIG_CMAQ_PATH}" | "${CSH_EXEC}" 2>&1 | tee -a "${LOG_FP}"
    # No! csh is running in a subshell, so its `setenv` are inaccessible ... except for echo:
#     for ECHO_LINE in $(echo -e "source ${CONFIG_CMAQ_PATH}" | "${CSH_EXEC}") ; do
    # nope, we're not getting its output. Instead: parse a file :-)

    TEMP_FP="$(mktemp)" # get a tempfile for output
    echo -e "source ${CONFIG_CMAQ_PATH}" | "${CSH_EXEC}" 2>&1 | tee -a "${TEMP_FP}"
    echo -e "${MESSAGE_PREFIX}   END: 'source ${CONFIG_CMAQ_PATH}' using '${CSH_EXEC}' to '${TEMP_FP}'" 2>&1 | tee -a "${LOG_FP}"

    while read -r ECHO_LINE ; do
      ECHO_PREFIX="${ECHO_LINE%% *}" # remove everything after first space
      if [[ "${ECHO_PREFIX}" == 'setenv' ]] ; then
        KEY_VAL="${ECHO_LINE#* }" # remove everything before first space
        KEY="${KEY_VAL%% *}" # remove everything after first space
        VAL="${KEY_VAL#* }" # remove everything before first space
#         echo -e "${MESSAGE_PREFIX} KEY='${KEY}' , VAL='${VAL}'" # debugging
        # now it gets ugly :-) and obviously brittle WRT config.cmaq
        # TODO: test for evaluation ('`') in VAL! This is dangerous ugly!
        case "${KEY}" in
#           'CC' ) CC="${VAL}" ; echo -e "${MESSAGE_PREFIX} CC='${CC}'" ;;
#           'compiler' ) compiler="${VAL}" ; echo -e "${MESSAGE_PREFIX} compiler='${compiler}'" ;;
#           'compiler_ext' ) compiler_ext="${VAL}" ; echo -e "${MESSAGE_PREFIX} compiler_ext='${compiler_ext}'" ;;
#           'CPP' ) CPP="${VAL}" ; echo -e "${MESSAGE_PREFIX} CPP='${CPP}'" ;;
#           'CPP_FLAGS' ) CPP_FLAGS="${VAL}" ; echo -e "${MESSAGE_PREFIX} CPP_FLAGS='${CPP_FLAGS}'" ;;
#           'C_FLAGS' ) C_FLAGS="${VAL}" ; echo -e "${MESSAGE_PREFIX} C_FLAGS='${C_FLAGS}'" ;;
          'EXEC_ID' ) EXEC_ID="${VAL}" ; echo -e "${MESSAGE_PREFIX} EXEC_ID='${EXEC_ID}'" ;;
#           'extra_lib' ) extra_lib="${VAL}" ; echo -e "${MESSAGE_PREFIX} extra_lib='${extra_lib}'" ;;
#           'FC' ) FC="${VAL}" ; echo -e "${MESSAGE_PREFIX} FC='${FC}'" ;;
#           'FP' ) FP="${VAL}" ; echo -e "${MESSAGE_PREFIX} FP='${FP}'" ;;
#           'F_FLAGS' ) F_FLAGS="${VAL}" ; echo -e "${MESSAGE_PREFIX} F_FLAGS='${F_FLAGS}'" ;;
#           'f_FLAGS' ) f_FLAGS="${VAL}" ; echo -e "${MESSAGE_PREFIX} f_FLAGS='${f_FLAGS}'" ;;
#           'f90_FLAGS' ) f90_FLAGS="${VAL}" ; echo -e "${MESSAGE_PREFIX} f90_FLAGS='${f90_FLAGS}'" ;;
#           'F90_FLAGS' ) F90_FLAGS="${VAL}" ; echo -e "${MESSAGE_PREFIX} F90_FLAGS='${F90_FLAGS}'" ;;
#           'IOAPI' ) IOAPI="${VAL}" ; echo -e "${MESSAGE_PREFIX} IOAPI='${IOAPI}'" ;;
#           'IOAPIMOD' ) IOAPIMOD="${VAL}" ; echo -e "${MESSAGE_PREFIX} IOAPIMOD='${IOAPIMOD}'" ;;
#           'LINKER' ) LINKER="${VAL}" ; echo -e "${MESSAGE_PREFIX} LINKER='${LINKER}'" ;;
#           'LINK_FLAGS' ) LINK_FLAGS="${VAL}" ; echo -e "${MESSAGE_PREFIX} LINK_FLAGS='${LINK_FLAGS}'" ;;
#           'M3LIB' ) M3LIB="${VAL}" ; echo -e "${MESSAGE_PREFIX} M3LIB='${M3LIB}'" ;;
#           'mpi' ) mpi="${VAL}" ; echo -e "${MESSAGE_PREFIX} mpi='${mpi}'" ;;
#           'MPI_INC' ) MPI_INC="${VAL}" ; echo -e "${MESSAGE_PREFIX} MPI_INC='${MPI_INC}'" ;;
#           'myCC' ) myCC="${VAL}" ; echo -e "${MESSAGE_PREFIX} myCC='${myCC}'" ;;
#           'myCFLAGS' ) myCFLAGS="${VAL}" ; echo -e "${MESSAGE_PREFIX} myCFLAGS='${myCFLAGS}'" ;;
#           'myFC' ) myFC="${VAL}" ; echo -e "${MESSAGE_PREFIX} myFC='${myFC}'" ;;
#           'myFFLAGS' ) myFFLAGS="${VAL}" ; echo -e "${MESSAGE_PREFIX} myFFLAGS='${myFFLAGS}'" ;;
#           'myFRFLAGS' ) myFRFLAGS="${VAL}" ; echo -e "${MESSAGE_PREFIX} myFRFLAGS='${myFRFLAGS}'" ;;
#           'myLINK_FLAG' ) myLINK_FLAG="${VAL}" ; echo -e "${MESSAGE_PREFIX} myLINK_FLAG='${myLINK_FLAG}'" ;;
#           'NETCDF' ) NETCDF="${VAL}" ; echo -e "${MESSAGE_PREFIX} NETCDF='${NETCDF}'" ;;
#           'NETCDF_DIR' ) NETCDF_DIR="${VAL}" ; echo -e "${MESSAGE_PREFIX} NETCDF_DIR='${NETCDF_DIR}'" ;;
        esac # case "${KEY}"
#          echo # newline debugging
      fi
    done < "${TEMP_FP}" # while read -r ECHO_LINE

  fi # [[ -r "${CONFIG_CMAQ_PATH}" ]]

  ### build vars usually set in bldit.cctm.
  export APPL='CCTM_CMAQv5_0_1' # string used in name of build dir, name of built executable
  CFG='CMAQ-BENCHMARK'
  export GRID_NAME="${CFG}"
  CCTM_EXEC_PREFIX="${APPL}"    # more mnemonic

  ### build dir--not THIS_DIR, but the space in which we build
  CCTM_BUILD_DIR_NAME="BLD_${CCTM_EXEC_PREFIX}"
  export CCTM_BUILD_DIR_PATH="${M3HOME}/${CCTM_BUILD_DIR_NAME}"
  # how to discover the target executable? more below, following were recovered *after* build
  CCTM_EXEC_FN="${CCTM_EXEC_PREFIX}_${EXEC_ID}"
#  export MODEL="${CCTM_EXEC_FN}" # Makefile wants this name
  export CCTM_EXEC_DIR="${CCTM_BUILD_DIR_PATH}"
  export CCTM_EXEC_FP="${CCTM_EXEC_DIR}/${CCTM_EXEC_FN}"

  ### just a synonym for M3DATA
  export INPUT_ROOT="${M3DATA}"

} # function setup_CCTM_common_vars

### Populate arrays used in runned:
### * BDOM: 0-based-Julian of first day of month, length=12
### * MDAY: |days in month|, length=12
### Depends on caller-set vars:
### * YEAR: YYYY
function setup_day_of_month_arrays {
  for N_THIS_MONTH in {1..12} ; do
    local I_MONTH=$(( ${N_THIS_MONTH} - 1 ))
    local FIRST_DAY_THIS_MONTH_YYYYMMDD="${YEAR}$(printf '%02d' ${N_THIS_MONTH})01"
    local FIRST_DAY_THIS_MONTH_JJJ="$(date -d "${FIRST_DAY_THIS_MONTH_YYYYMMDD}" +%j)"
    ## format of FIRST_DAY_THIS_MONTH_JJJ is %03, and leading zero looks octal,
    ## so gotta strip the leading zeros
    local FIRST_DAY_THIS_MONTH_J="$(echo -e ${FIRST_DAY_THIS_MONTH_JJJ} | sed -e 's/^0*//g')"
    # first day of month as zero-based Julian: i.e., bdom[0]==0 (julian(1 Jan)==0
    BDOM[I_MONTH]="$(( ${FIRST_DAY_THIS_MONTH_J} - 1 ))"
    local FIRST_DAY_NEXT_MONTH="$(date -d "${FIRST_DAY_THIS_MONTH_YYYYMMDD} +1 month")"
    local LAST_DAY_THIS_MONTH_DD="$(date -d "${FIRST_DAY_NEXT_MONTH} -1 day" +%d)"
    # number of days in this month
    MDAY[I_MONTH]="${LAST_DAY_THIS_MONTH_DD}"
#     # start debugging
#     echo -e "  0-based Julian first of month='${N_THIS_MONTH}'->'${BDOM[I_MONTH]}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
#     echo -e "      1-based last day of month='${N_THIS_MONTH}'->'${MDAY[I_MONTH]}'" 2>&1 | tee -a "${RUNNER_LOG_FP}"
#     echo 2>&1 | tee -a "${RUNNER_LOG_FP}" # newline
#    #   end debugging
  done # for N_THIS_MONTH in {1..12}
} # function setup_day_of_month_arrays
