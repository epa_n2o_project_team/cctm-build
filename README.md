*(part of the [AQMEII-NA_N2O][AQMEII-NA_N2O wiki home] family of projects)*

**table of contents**

[TOC]

# open-source notice

Copyright 2013, 2014 Tom Roche <Tom_Roche@pobox.com>

This project's content is free software: you can redistribute it and/or modify it provided that you do so as follows:

* under the terms of the [GNU Affero General Public License][GNU Affero General Public License HTML @ GNU] as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

This project's content is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU Affero General Public License][GNU Affero General Public License local text] for more details.

![distributed under the GNU Affero General Public License](../../downloads/Affero_badge__agplv3-155x51.png)

[GNU Affero General Public License local text]: ./COPYING
[GNU Affero General Public License HTML @ GNU]: https://www.gnu.org/licenses/agpl.html

# overview

Project for building and running [version=5.0.1][CMAQ-5.0.1 @ CMAS wiki] of the [CMAQ chemistry-transport model][CCTM @ CMAS wiki] (aka "the model"). Requirements include:

1. Source code is in the [submodules][submodules @ the Git book] [CCTM][CCTM @ bitbucket], [ICL][ICL @ bitbucket], [MECHS][MECHS @ bitbucket].
1. [BLDMAKE][BLDMAKE @ bitbucket] is also required for the [CMAQ model builder][bldmake @ CMAS wiki].
1. Über-configuration files are in project=[`CMAQ-build`][CMAQ-build @ bitbucket]:
    * for a `bash`-based build:
        * [`uber.config.cmaq.sh`][CMAQ-build>Source>uber.config.cmaq.sh]
        * [`config.cmaq.sh`][CMAQ-build>Source>config.cmaq.sh]
    * for a `csh`-based build:
        * [`uber.config.cmaq.csh`][CMAQ-build>Source>uber.config.cmaq.csh]
        * [`config.cmaq`][CMAQ-build>Source>config.cmaq]

[AQMEII-NA_N2O wiki home]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Home
[CMAQ-5.0.1 @ CMAS wiki]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD
[CCTM @ CMAS wiki]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD#CMAQ_Chemistry-Transport_Model_.28CCTM.29
[submodules @ the Git book]: http://git-scm.com/book/en/Git-Tools-Submodules
[CCTM @ bitbucket]: https://bitbucket.org/epa_n2o_project_team/cctm
[ICL @ bitbucket]: https://bitbucket.org/epa_n2o_project_team/icl
[MECHS @ bitbucket]: https://bitbucket.org/epa_n2o_project_team/mechs
[BLDMAKE @ bitbucket]: https://bitbucket.org/epa_n2o_project_team/bldmake
[bldmake @ CMAS wiki]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD#Model_Builder_.28Bldmake.29
[CMAQ-build @ bitbucket]: https://bitbucket.org/epa_n2o_project_team/cmaq-build/
[CMAQ-build>Source>uber.config.cmaq.csh]: https://bitbucket.org/epa_n2o_project_team/cmaq-build/HEAD/uber.config.cmaq.csh
[CMAQ-build>Source>uber.config.cmaq.sh]: https://bitbucket.org/epa_n2o_project_team/cmaq-build/HEAD/uber.config.cmaq.sh
[CMAQ-build>Source>config.cmaq]: https://bitbucket.org/epa_n2o_project_team/cmaq-build/HEAD/config.cmaq
[CMAQ-build>Source>config.cmaq.sh]: https://bitbucket.org/epa_n2o_project_team/cmaq-build/HEAD/config.cmaq.sh

# operation

## setup

The code is currently setup to run (and has successfully run) on the [EPA AMAD][] HPCC host=`infinity`. To run there (presuming that

* not much has changed
* your permissions are like mine
* lots of other assumptions hold that probably don't :-)

you can skip the first step.

1. Setup (with consistent compile/link settings!) the [required libraries][CMAQ-5 required libraries @ CMAS wiki] on your build machine, to which one must point with `M3LIB`. This will probably be your major challenge; fortunately, the fine folks on [m3user@listserv.unc.edu][m3user@listserv.unc.edu main page] are often helpful with that.
1. Setup your build space. This step can be automated with [`build_CCTM_driver.sh`][CCTM-build>Source>build_CCTM_driver.sh].
    1. Create some root directory/folder for your build (e.g., `/tmp/CMAQ`) and move to it. This root folder will become your `M3HOME`, hence we will denote its path thusly.
    1. Into your `${M3HOME}`, [clone][git cloning @ AQMEII-NA_N2O wiki] as peers (i.e., both first-level-subfolders of the root folder)
        1. [this project=CCTM-build][CCTM-build @ bitbucket]
        1. [project=CMAQ-build][CMAQ-build @ bitbucket]
1. Edit build properties.
    1. Edit your [`${M3HOME}/CCTM-build/CCTM_utilities.sh`][CCTM-build>Source>CCTM_utilities.sh] so as to
        * properly set CCTM-wide configuration variables (e.g., `APPL`).
        * determine which of the following `*config.cmaq*` files to use.
    1. Edit your über-config files:
        * These files set your CMAQ first-order über-configuration variables (e.g., `M3LIB`). There are two:
            * [`${M3HOME}/CMAQ-build/uber.config.cmaq.sh`][CMAQ-build>Source>uber.config.cmaq.sh]
            * [`${M3HOME}/CMAQ-build/uber.config.cmaq.csh`][CMAQ-build>Source>uber.config.cmaq.csh]
        * It should only be necessary to edit one, depending on whether you want to build with `bash` (which you should) or `csh` (if you are tradition-bound). Unfortunately, presently, our build processes are a mixture of both: not all the `bldit*` scripts have been rewritten `bash`-style.
    1. Edit your [`${M3HOME}/CMAQ-build/config.cmaq.sh`][CMAQ-build>Source>config.cmaq.sh] or [`${M3HOME}/CMAQ-build/config.cmaq`][CMAQ-build>Source>config.cmaq] (`csh`-based)
        * as determined by your [`CCTM_utilities.sh`][CCTM-build>Source>CCTM_utilities.sh]
        * so as to properly set your CMAQ second-order configuration variables (e.g., compile/link flags).
        * Note that currently [`CCTM_utilities.sh`][CCTM-build>Source>CCTM_utilities.sh] is set to use [`uber.config.cmaq.csh`][CMAQ-build>Source>uber.config.cmaq.csh], since we're using [`bldmake`][bldmake @ CMAS wiki], which expects `csh`.

[EPA AMAD]: http://www.epa.gov/AMD/
[CMAQ-5 build configuration @ CMAS wiki]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD#Configuring_your_system_for_compiling_CMAQ
[CMAQ-build>Source>config.cmaq>Raw]: https://bitbucket.org/epa_n2o_project_team/cmaq-build/raw/HEAD/config.cmaq
[CMAQ-5 required libraries @ CMAS wiki]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#Required_Libraries
[m3user@listserv.unc.edu main page]: http://lists.unc.edu/read/?forum=m3user
[git cloning @ AQMEII-NA_N2O wiki]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Cloning_a_git_repository
[CCTM-build @ bitbucket]: https://bitbucket.org/epa_n2o_project_team/cctm-build
[CCTM-build>Source]: ../../src
[CCTM-build>Source>CCTM_utilities.sh]: ../../src/HEAD/CCTM_utilities.sh
[CCTM-build>Source>build_CCTM_driver.sh]: ../../src/HEAD/build_CCTM_driver.sh

## build CCTM

1. Edit your CCTM builder [`${M3HOME}/CCTM-build/build_CCTM.sh`][CCTM-build>Source>build_CCTM.sh] to appropriately set build-specific configuration variables for your platform and runtime specifications.
1. Run your saved `${M3HOME}/CCTM-build/build_CCTM.sh` to build your CCTM.

[CCTM-build>Source>build_CCTM.sh]: ../../src/HEAD/build_CCTM.sh
[CCTM-build>Source>build_CCTM.sh>Raw]: ../../raw/HEAD/build_CCTM.sh
[CCTM-build>Source>bldit.cctm]: ../../src/HEAD/bldit.cctm

## run CCTM

1. Take a look at your [`${M3HOME}/CCTM-build/outck.q`][CCTM-build>Source>outck.q]: it doesn't do much, but you may need to edit it. (Until I make it go away!)
1. Edit your CCTM runner [`${M3HOME}/CCTM-build/run.cctm`][CCTM-build>Source>run.cctm] to appropriately set run-specific configuration variables for your platform and runtime specifications.
    * If doing several similar runs, you may choose to also use higher-level "runner drivers" like [`${M3HOME}/CCTM-build/run.cctm_runner.sh`][CCTM-build>Source>run.cctm_runner.sh].
1. Run your saved `run.cctm` (or drivers) to run your CCTM.

[CCTM-build>Source>outck.q]: ../../src/HEAD/outck.q
[CCTM-build>Source>run.cctm]: ../../src/HEAD/run.cctm
[CCTM-build>Source>run.cctm_runner.sh]: ../../src/HEAD/run.cctm_runner.sh

## analyze output

If you want to compare your run's output with "golden" reference values (e.g., for the [CMAQ benchmark][benchmarking @ CMAQ wiki]), consider using code like [`bias_probe_driver_driver.sh`][CCTM-build>Source>bias_probe_driver_driver.sh] to drive [`bias_probe_driver.sh`][CCTM-build>Source>bias_probe_driver.sh] to drive [`bias_probe.ncl`][CCTM-build>Source>bias_probe.ncl] to "probe" your "biases" (i.e., the differences between your actual and reference output).

[benchmarking @ CMAQ wiki]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#Benchmarking
[CCTM-build>Source>bias_probe_driver_driver.sh]: ../../src/HEAD/bias_probe_driver_driver.sh
[CCTM-build>Source>bias_probe_driver.sh]: ../../src/HEAD/bias_probe_driver.sh
[CCTM-build>Source>bias_probe.ncl]: ../../src/HEAD/bias_probe.ncl

# versions

I'm currently versioning using git branching, I currently branch the entire [`CMAQ-build` project family][CMAQ-build project family table] when I do so, using [`repo_branch.sh`][repo_branch.sh @ bitbucket].

[CMAQ-build project family table]: https://bitbucket.org/epa_n2o_project_team/cmaq-build#rst-header-cmaq-build-project-family
[repo_branch.sh @ bitbucket]: https://bitbucket.org/epa_n2o_project_team/cmaq-build/src/HEAD/repo_branch.sh?at=master

## CMAQ-5.0.1 benchmark

Analysands above were from benchmark run of build from branch=`CMAQ-5.0.1_benchmark_runs_Nov_2013`.

## AQMEII-NA_N2O_2008

The initial run of [`AQMEII-NA_N2O_2008`][AQMEII-NA_N2O wiki homepage @ bitbucket] used branch=`AQMEII-NA_N2O_2008_0` (which is also `master`, at least for now).

[AQMEII-NA_N2O wiki homepage @ bitbucket]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Home

# TODOs

1. Move all these TODOs to [issue tracker][CCTM-build issues]
1. Make build fully `bash`-based. Probably requires changes to `bldmake` or `bldit.cctm`, since they expect `csh` (?)
1. Implement `run_CCTM.sh` (**very** preliminary non-version saved [here][run_CCTM.sh upload (not source) @ bitbucket], just so I don't lose it) for both {CMAQ benchmark, "real" run}.
1. `bias_probe*` issues:
    1. write analysis spreadsheets programmatically, add finer-grained stats (e.g., deciles instead of quartiles), add variation measures (e.g., σ)
    1. write the calculated relative bias, use that to plot (in R?) **locations** with large biases
    1. integrate bias_probe* into `run_CCTM.sh`
1. Refactor away [`bldit.cctm`][CCTM-build>Source>bldit.cctm]: it should already be vestigial WRT envvar setting, so just do (in [`build_CCTM.sh`][CCTM-build>Source>build_CCTM.sh]) whatever `bldit.cctm` is doing that `build_CCTM.sh` is not already doing.
1. Refactor away [`outck.q`][CCTM-build>Source>outck.q]: it doesn't do much, so just do in [`run.cctm`][CCTM-run>Source>run.cctm] (and ultimately `run_CCTM.sh`--see above) whatever `outck.q` is doing that `run.cctm` is not already doing.

[CCTM-build issues]: ../../issues
[run_CCTM.sh upload (not source) @ bitbucket]: ../../downloads/run_CCTM.sh
[CCTM-run>Source>run.cctm]: ../../src/HEAD/run.cctm?at=master
